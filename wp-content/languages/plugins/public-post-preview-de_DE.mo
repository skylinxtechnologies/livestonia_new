��          �      �       0     1  	   M     W     i       i   �     �          #     2     M  >   a  �   �      �     �     �      �  
   �  �   
  '   �     �     �      �        ;           	                
                                  (Copy and share this link.) Disabled! Dominik Schilling Enable public preview Enabled! Enables you to give a link to anonymous users for public preview of any post type before it is published. No Public Preview available! Public Post Preview Public Preview The link has been expired! http://wphelper.de/ https://dominikschilling.de/wp-plugins/public-post-preview/en/ PO-Revision-Date: 2015-10-29 21:32:04+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.1.0-alpha
Project-Id-Version: Stable (latest release)
 (Kopiere und teile diesen Link.) Deaktiviert! Dominik Schilling Öffentliche Vorschau aktivieren Aktiviert! Bietet die Möglichkeit einen Link mit Dritten zu Teilen, um eine Vorschau eines bisher unveröffentlichten Inhaltstyp zu geben. Keine öffentliche Vorschau verfügbar! Öffentliche Vorschau Öffentliche Vorschau Der Link ist nicht mehr gültig! http://wphelper.de/ https://dominikschilling.de/wp-plugins/public-post-preview/ 
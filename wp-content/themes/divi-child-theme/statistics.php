<?php 
/**
* Template Name: Stats
* Description:stats page
*/

global $current_user;
wp_get_current_user(); 

get_header();

if ( current_user_can( 'administrator' ) ) {
	?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped" align="center">
					<caption> <h1>Statistics</h1></caption>
					<tbody>
						<tr>	<td>Total Users</td>
							<td><?php
								$result = count_users();
								echo  $result['total_users'];
								?></td>
							</tr>
						<tr>
<td>Total Events</td>
<td><?php
									global $wpdb;
									$table_name = $wpdb->prefix . "em_events";
									$event_scount = $wpdb->query("SELECT * FROM ".$table_name );
									echo $event_scount;?></td>
</tr>
						</tbody>
					</table>				
		
						<table class="table table-striped">
							<caption> <h1>Total Post </h1></caption>
							<thead>
								<tr>
									<th>Name</th>
									<th>Posts</th>
								</tr>
							</thead>
							<tbody>
								<?php $published_posts = wp_count_posts(); 
								foreach ($published_posts as $name => $total) 

								{
									if($total != 0 ){
										?>	
										<tr>
											<td class="title column-title has-row-actions column-primary page-title"><?php echo $name;?></td>
											<td class="title column-title has-row-actions column-primary page-title"><?php echo $total;?></td>
										</tr>

										<?php
									}
								}
								?>
							</tbody>
						</table>


					</div>
				</div>
			</div>			
			<?php
		}
		?>
		<div class="container">
			<div clas="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<caption><h1>Blogs</h1> <a class="btn btn-primary" href="<?php echo site_url('english-custom-post/') ?>">Add New</button></a>	</caption>
						<thead>
							<tr>
								<th>Image</th>
								<th>Title</th>
								<th>Author</th>
								<th>Actions</th>	
								<th>Status</th>	
							</tr>
						</thead>	
						<tbody>
							<?php
							global $wpdb;
							global $post;
							global $current_user; 

							get_currentuserinfo(); 
							$table = $wpdb->prefix . "posts";
							$str = "SELECT $wpdb->posts.* FROM ".$table." WHERE post_type = 'post' ORDER BY post_date DESC ";


							$result = $wpdb->get_results($str);
							if ( current_user_can( 'administrator' ) ) {
								foreach($result as $post){
									setup_postdata($post);
									?>

									<tr>
										<td><?php the_post_thumbnail(array('height'=>70,'width'=>50));?></td>
										<td><span><a href="<?php the_permalink();?>"><?php the_title();?></span></td>
										<td><span><?php the_author();?></span></td>
										<td><?php edit_post_link(__('Edit Post')); ?></td>
										<td><?php echo get_post_status() ?></td>
									</tr>
									<?php 
								}
							}
							else
							{
								foreach($result as $post){
									setup_postdata($post);
									if(get_the_author_id() == $current_user->ID){
										?>
										<tr>
											<td><?php the_post_thumbnail(array('height'=>70,'width'=>50));?></td>
											<td><span><a href="<?php the_permalink();?>"><?php the_title();?></span></td>
											<td><span><?php the_author();?></span></td>
											<td><?php edit_post_link(__('Edit Post')); ?></td>
											<td><?php echo get_post_status() ?></td>

										</tr>
										<?php 
									}
								}
							}
							?>
						</tbody>
					</table>
					<table class="table table-striped">
						<caption><h1>Events</h1></caption>

						<thead>
							<tr>
								<th>Event Name</th>
								<th>Even start Date</th>
								<th>Even end Date</th>
								<th>Action</th>
							</tr>
						</thead>	
						<tbody>
							<?php
							if ( current_user_can( 'administrator' ) ){
								global $wpdb;
								global $post;

								$post_table = $wpdb->prefix ."posts";
								$table = $wpdb->prefix ."em_events";


								$str = "SELECT * FROM ".$post_table." 
								INNER JOIN ".$table." ON ".$post_table.".ID= ".$table.".post_id
								WHERE post_type='event' ORDER BY post_date DESC " ;

								$result = $wpdb->get_results($str);

								foreach($result as $events){
									$post=$events;
									setup_postdata($post);
									?>
									<tr>
										<td><?php echo the_title(); ?>	</td>
										<td><?php echo $events->event_start_date ?></td>
										<td><?php echo $events->event_end_date ?></td>					
										<td><a href="<?php echo get_site_url()?>/wp-admin/post.php?post=<?php echo $events->post_id?>&action=edit">Edit	post</a></td>
									</tr>
									<?php 
								}
							}
							else
							{
								global $wpdb;
								global $post;
								global $current_user; 
								get_currentuserinfo();
								$post_table = $wpdb->prefix . "posts";
								$table = $wpdb->prefix . "em_events";


								$str = "SELECT * FROM ".$post_table." 
								INNER JOIN ".$table." ON ".$post_table.".ID= ".$table.".post_id
								WHERE post_type='event' AND event_owner=".$current_user->ID ;

								$result = $wpdb->get_results($str);
								?>
								<tbody>
									<?php
									foreach($result as $events){
										$post=$events;
										setup_postdata($post);


										?>
										<tr>
											<td class=""><?php  echo the_title();?></td>
											<td><?php echo $events->event_start_date ?></td>
											<td><?php echo $events->event_end_date ?></td>
											<td><a href="<?php echo get_site_url()?>/wp-admin/post.php?post=<?php echo $events->post_id?>&action=edit">Edit post</a></td>

										</tr>

										<?php 
									}

								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

	<<style type="text/css" media="screen">
		
	body.admin-bar.et_fixed_nav #main-header {
		top: 0px	;
	}

	</style>

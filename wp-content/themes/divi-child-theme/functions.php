	<?php
	/*
	* Divi child theme custom functions
	* 
	*/


	/**
	* Enqueue styles and scripts into WordPress theme
	* 
	* @return null
	*/

	function dc_enqueue()
	{
	wp_register_style('bootstrapcdn', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
	wp_enqueue_style('bootstrapcdn');

	wp_register_script('bootstrapjs', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', ['jquery'], '3.3.7', true);
	wp_enqueue_script('bootstrapjs');

	wp_register_script('child-script', get_stylesheet_directory_uri().'/script.js', ['jquery'], '1.0.0', true);
	wp_enqueue_script('child-script');
	}
	add_action('wp_enqueue_scripts', 'dc_enqueue');
	//responding to ajax request
	add_action('wp_ajax_nopriv_update_bio', 'dc_update_bio');
	add_action('wp_ajax_update_bio', 'dc_update_bio');

	/**
	* Update user profile biography field
	* 
	* @return null
	*/

	function dc_update_bio()
	{
	$text = esc_attr( $_POST['bio'] );
	update_user_meta(get_current_user_id(), 'description', esc_attr( $_POST['bio'] ));
	echo $text;
	}

	/**
	* Display custom user fields
	* 
	* @param Object $current_author author object
	* 
	* @return String
	*/

	function dc_add_author_extra_fields($current_author)
	{
	if( $current_author->user_description )
	{
	if( get_current_user_id() == $current_author->ID )
	{
	echo '<h3>About</h3>';
	echo '<p>' . $current_author->user_description . '</p>';
	}
	}
	
	echo '<br></br>';
	echo '<h3>Contact</h3>';
	
	if( $skype = get_the_author_meta('skype_username', $current_author->ID) )
	{
	echo '<p class="author-info">Skype: <a href="skype:'. $skype . '?call" >'. $skype . '</a></p>';
	}
	if( $facebook = get_the_author_meta('facebook_username', $current_author->ID) )
	{
	echo '<p class="author-info">Facebook: <a target="_blank" href="http://fb.com/'. $facebook . '" >'. $facebook . '</a></p>';
	}
	if( $phone = get_the_author_meta('phone_number', $current_author->ID) )
	{
	echo '<p class="author-info">Phone: <a href="tel:'. $phone . '" >'. $phone . '</a></p>';
	}

	echo '<p class="author-info">Email: <a href="mailto:'. $current_author->user_email .'" >'. $current_author->user_email .'</a></p>';

	if( $address = get_the_author_meta('address', $current_author->ID) )
	{
	echo '<p class="author-info">Address: '. $address . '</p>';
	}
	echo '<br></br>';
	
	}


	/**
	* Display user introductory youtube video
	* 
	* @param Object $current_author author object
	* 
	* @return String
	*/

	function dc_add_author_intro_video($current_author)
	{
	if( get_the_author_meta('youtube_video_url', $current_author->ID ) )
	{
	$url = get_the_author_meta('youtube_video_url', $current_author->ID );
	preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
	$yid = $matches[1];
	$width = '560px';
	$height = '315px';
	?>
	<h2>Introductory Video</h2>
	<iframe width="<?php echo $width; ?>" height="<?php echo $height; ?>"  frameborder="0" allowfullscreen src="https://www.youtube.com/embed/<?php echo $yid; ?>"></iframe>
	<?php
	}
	}

	/**
	* Fetch post by special tag name
	* 
	* @param string $tag tag name for filtering post
	* 
	* @return String
	*/

	function dc_posts_by_tag($tag=null)
	{
	if(!is_null($tag))
	{
	$the_query = new WP_Query( ['tag' => $tag, 'posts_per_page' => 1 , 'orderby' => 'post_date', 'order' => 'DESC'] );

	if( $the_query->have_posts() )
	{
	while( $the_query->have_posts() )
	{
	$the_query->the_post();
	echo '<p class="post-title">'.get_the_title().'</p>';
	echo '<p class="post-excerpt">'.substr(get_the_excerpt(), 0, 45).'..</p>';
	}
	}
	wp_reset_postdata();
	}
	
	}

	/**
	* Fetch post link by special tag name
	* 
	* @param string $tag tag name for filtering post
	* 
	* @return String
	*
	*/
	function dc_link_of_posts_by_tag($tag=null)
	{
	if(!is_null($tag))
	{
	$the_query = new WP_Query( ['tag' => $tag, 'posts_per_page' => 1 , 'orderby' => 'post_date', 'order' => 'DESC'] );

	if( $the_query->have_posts() )
	{
	while( $the_query->have_posts() )
	{
	$the_query->the_post();
	return get_permalink();
	}
	}
	wp_reset_postdata();
	}
	}

	/**
	* Fetch background/attachment image of post by special tag
	* 
	* @param string $tag tag name for filtering post
	* 
	* @return String
	*/

	function dc_thumbnail_posts_by_tag($tag=null)
	{
	if(!is_null($tag))
	{
	$the_query = new WP_Query( ['tag' => $tag, 'posts_per_page' => 1 , 'orderby' => 'post_date', 'order' => 'DESC'] );

	if( $the_query->have_posts() )
	{
	while( $the_query->have_posts() )
	{
	$the_query->the_post();
	
	echo wp_get_attachment_url( get_post_thumbnail_id() );
	}
	}
	wp_reset_postdata();
	}
	}

	/**
	* Fetch latest posts filter by numbers
	* 
	* @return String
	*/

	function dc_show_latest_blog_posts()
	{
	$limit = 2;
	$order = 'desc';

	$post_block_options = get_option('po_block_settings');
	if(isset($post_block_options)){
	$post_block_options = $post_block_options['po_post_block'];

	$limit = ($post_block_options['no_of_posts_to_show'] < 2 ) ? 2 : $post_block_options['no_of_posts_to_show'];
	$order = $post_block_options['post_order'];
	}
	
	
	$args = array(
	'numberposts' => $limit,
	'offset' => 0,
	'category' => 0,
	'orderby' => 'post_date',
	'order' => strtoupper($order),
	'include' => '',
	'exclude' => '',
	'meta_key' => '',
	'meta_value' =>'',
	'post_type' => 'post',
	'post_status' => 'publish',
	'suppress_filters' => true
	);

	$recent_posts = wp_get_recent_posts( $args );
	$count = 0;
	foreach($recent_posts as $post)
	{
	$count++;
	$the_query = new WP_Query( [ 'p' => $post['ID'] ] );
	$the_query->the_post();
	if($limit%2 != 0 && $count == $limit){
	?>
	<div class="col-sm-12">
	<a href="<?php echo get_permalink(); ?>">
	<section class="post-section" style="background: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>'); background-size:cover; background-repeat:no-repeat;">
	<div class="post-group">
	<p class="post-title"><?php echo get_the_title(); ?></p>
	<p class="post-excerpt"><?php echo substr(get_the_excerpt(), 0, 45); ?>..</p>
	</div>
	</section>
	</a>
	</div>
	<?php
	}else{
	?>
	<div class="col-sm-6">
	<a href="<?php echo get_permalink(); ?>">
	<section class="post-section" style="background: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>'); background-size:cover; background-repeat:no-repeat;">
	<div class="post-group">
	<p class="post-title"><?php echo get_the_title(); ?></p>
	<p class="post-excerpt"><?php echo substr(get_the_excerpt(), 0, 45); ?>..</p>
	</div>
	</section>
	</a>
	</div>
	<?php
	}
	}
	}

	/**
	* Fill the newsletter placeholders with related post meta information
	*
	* @param Object $newsletter_obj newsletter object
	* @param Object $post_obj post object
	*
	* @return String
	*/

	function dc_publish_newsletter_content($newsletter_obj, $post_obj)
	{
	if( has_post_thumbnail() ){
	$thumbnail = get_the_post_thumbnail();
	}else{
	$thumbnail = '';
	}
	
	$matches = [
	'###POSTTITLE###'          => get_the_title(),
	'###POSTLINK###'           => '<a href="'.get_post_permalink().'" class="outer-link">'.get_post_permalink().'</a>',
	'###POSTIMAGE###'          => '<a href="'.get_post_permalink().'">'.$thumbnail.'</a>',
	'###POSTDESC###'	       => get_the_excerpt(),
	'###POSTFULL###'	       => nl2br(get_the_content()),
	'###DATE###'		       => get_the_date('Y-m-d h:i:s'),
	'###POSTLINK-WITHTITLE###' => '<a href="'.get_post_permalink().'" class="outer-link">'.get_the_title().'</a>',
	'###POSTLINK-ONLY###'      => '<a href="'.get_post_permalink().'" class="outer-link">'.get_post_permalink().'</a>',
	//'###AUTHOR###'			   => '<p class="author-name">Posted by <a href="' . get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) . '">'.get_the_author_meta( 'user_nicename' ).'</a></p>'

	];

	$newsletter_body  = nl2br($newsletter_obj[0]->es_templ_body);
	$newsletter_heading = nl2br($newsletter_obj[0]->es_templ_heading); 
	
	//process newsletter heading
	$newsletter_heading = preg_replace_callback('/###[\S]+###/', function($input) use($matches){
	
	if(array_key_exists($input[0], $matches))
	return $matches[$input[0]];

	}, $newsletter_heading);

	//show newsletter heading text
	echo '<h2>' . $newsletter_heading . '</h2>';

	//process newsletter body
	$newsletter_body = preg_replace_callback('/###[\S]+###/', function($input) use($matches){
	
	if(array_key_exists($input[0], $matches))
	return $matches[$input[0]];

	}, $newsletter_body);

	//show newsletter body text
	echo $newsletter_body;

	}

	/**
	* Register widget sidebars
	*
	* @param array $args configuration array
	*/

	if( function_exists('register_sidebar') )
	{
	
	$args = array(
	'name'          => 'Front Right Widget',
	'id'            => 'front-sidebar-right',
	'description'   => 'The panel for front page right sidebar',
	'class'         => '',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h4>',
	'after_title'   => '</h4>'
	);
	
	register_sidebar( $args );
	
	}

	/**
	*
	* Custom Modules
	*
	*/
	function ds_custom_modules()
	{
	if( class_exists('ET_Builder_Module') ){
	include 'ds-custom-modules.php';
	}
	}

	function Prep_ds_custom_modules()
	{
	global $pagenow;

	$is_admin = is_admin();
	$action_hook = $is_admin ? 'wp_loaded' : 'wp';

	$required_admin_pages = array('edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php', 'export.php');
	$specific_filter_pages = array('edit.php', 'admin.php', 'edit-tags.php');
	$is_edit_library_page = 'edit.php' == $pagenow && isset( $_GET['post_type'] ) && 'et_pb_layout' === $_GET['post_type'];
	$is_role_editor_page = 'admin.php' === $pagenow && isset( $_GET['page'] ) && 'et_divi_role_editor' === $_GET['page'];
	$is_import_page = 'admin.php' === $pagenow && isset( $_GET['import'] ) && 'wordpress' === $_GET['import']; 
	$is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset( $_GET['taxonomy'] ) && 'layout_category' === $_GET['taxonomy'];

	if ( ! $is_admin || ( $is_admin && in_array( $pagenow, $required_admin_pages ) && ( ! in_array( $pagenow, $specific_filter_pages ) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page ) ) ) {
	add_action($action_hook, 'DS_Custom_Modules', 9789);
	}

	}
	Prep_ds_custom_modules();

	/**
	*
	* Remove tag from a previous post with same category
	*
	*/
	function ds_remove_tag_from_previous_post()
	{
	//#1 solution
	/*$tags  = get_tags(['hide_empty'=>true, 'name__like'=>'premium']);
	foreach($tags as $tag){
	$query = new WP_Query(['tag_id'=>$tag->term_id, 'orderby'=>'modified']);
	if(count($query->posts) != 1){
	$previous_post = end($query->posts);
	wp_remove_object_terms( $previous_post->ID, $tag->slug , 'post_tag');
	}
	}*/

	//#2 solution
	$categories = get_categories(['hide_empty'=>true, 'orderby'=>'name', 'parent'=>0]);
	$tags  = get_tags(['hide_empty'=>true, 'name__like'=>'premium']);
	$tags_array = array();
	$posts_array = array();
	
	foreach($tags as $tag){$tags_array[] = $tag->term_id;}
	
	foreach($categories as $category){
	$first_query = new WP_Query(['cat_id'=>$category->term_id, 'orderby'=>'modified']);

	foreach($first_query->posts as $post){$posts_array[] = $post->ID;}

	foreach($tags_array as $tag){
	$second_query = new WP_Query(['post__in' => $posts_array, 'tag_id' => $tag, 'orderby'=>'modified']);
	if(count($second_query->posts) > 1){
	$previous_post = end($second_query->posts);
	wp_remove_object_terms( $previous_post->ID, $tag , 'post_tag');
	}
	}
	$posts_array = array();
	}
	}
	add_action('admin_init', 'ds_remove_tag_from_previous_post');

	/**
	*
	* Activate plugin for subscriber user role
	*
	*/
	function ds_activate_tags_meta_transformation_plugin()
	{
	global $current_user;
	if(in_array('subscriber', $current_user->roles)){
	//user is subscriber
	$result = activate_plugins(
	array('/transform-meta-boxes/transform-meta-boxes.php'),
	'',
	false,
	true
	);
	if(is_wp_error($result)){
	print_r('A problem occured:' . $result);
	}
	}else{
	deactivate_plugins(
	array('/transform-meta-boxes/transform-meta-boxes.php'),
	true
	);
	}

	}
	add_action('admin_init', 'ds_activate_tags_meta_transformation_plugin');

	/**
	*
	* Execute script to remove premium tags from subscriber
	*
	*/
	function ds_remove_premium_admin_footer($data)
	{
	global $current_user;
	if(in_array('subscriber', $current_user->roles)){
	//user is subscriber
	echo "<script>
	jQuery(\"#tagsdiv-post_tag\").find(\"input[type='checkbox'][value*='premium']\");
	jQuery(\"#tagsdiv-post_tag\").find(\"label:contains('premium')\").remove();
	</script>";
	}
	}
	add_action('admin_footer', 'ds_remove_premium_admin_footer', 10, 1);

	/*
	* Show youtube video on author page
	*/
	function dc_add_youtube_video($current_author)
	{
	if( get_the_author_meta('youtube_video_url', $current_author->ID ) )
	{
	$url = get_the_author_meta('youtube_video_url', $current_author->ID );
	preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
	$yid = $matches[1];
	$width = '500';
	$height = '281';
	?>
	<iframe id="youtube_iframe" width="<?php echo $width; ?>" height="<?php echo $height; ?>"  frameborder="0" allowfullscreen src="https://www.youtube.com/embed/<?php echo $yid; ?>"></iframe>
	<?php
	}
	}

	/*class DS_Custom_Tag
	{
	public function __construct(){
	$tags  = get_tags(['hide_empty'=>true, 'name__like'=>'premium']);
	$tags_array = array();
	
	foreach($tags as $tag){$tags_array[] = $tag->term_id;}
	
	$this->terms = get_terms('post_tag', array(
	'orderby'			=> 'name',
	'hide_empty'		=> false,
	'exclude'			=> '',
	));
	}

	public function toggles($post){
	$this->selected = $this->get_selected($post->ID);
	$output = '<div id="taxonomy-post_tag" class="categorydiv checkbox-toggles">'
	. $this->display_buttons_recursively($this->terms)
	. '</div>';
	echo $output;
	}

	public function display_buttons_recursively($terms = array(), $level = 0){
	$output = '';
	foreach ($terms as $i => $term) {
	$checked = (in_array($term->term_id, $this->selected)) ? ' checked="checked"' : '';
	$output.= '<input type="checkbox" name="post_tag[]" id="post_tag['.$i.']" value="'.$term->term_id.'"'.$checked.'>'
	. '<label class="level-'.$level.'" for="post_tag['.$i.']">'.$term->name.' ('.$term->count.')</label>';
	if (isset($term->children) AND sizeof($term->children)) $output.= $this->display_buttons_recursively($term->children, $level+1);
	}
	return $output;
	}

	private function get_selected($post_id) {
	$post_has = wp_get_object_terms($post_id,'post_tag');
	$selected = array(); foreach ($post_has as $term) $selected[] = $term->term_id;
	return $selected;
	}
	}*/
	/**
	*
	* Change tags meta box while using current screen
	*
	*/
	/*function ds_modify_tags_meta_box()
	{
	$tax_obj = get_taxonomy('post_tag');
	$tax_obj->meta_box_cb = array(new DS_Custom_Tag(), 'toggles');

	register_taxonomy('post_tag', $tax_obj->object_type, (array)$tax_obj);
	}*/
	//add_action('init', 'ds_modify_tags_meta_box',999);


	add_action('save_post','save_post_callback');
	function save_post_callback($post_id){
	global $post; 
	$user = wp_get_current_user();
	//var_dump($user->roles);
	$can_edit = (in_array('administrator',$user->roles));
	//var_dump($can_edit);
	if(!$can_edit){
	wp_remove_object_terms( $post_id, 'event-premium', 'post_tag' );
	wp_remove_object_terms( $post_id, 'tv-premium', 'post_tag' );
	wp_remove_object_terms( $post_id, 'blog-premium', 'post_tag' );
	wp_remove_object_terms( $post_id, 'radio-premium', 'post_tag' );
	}
	//var_dump(has_tag('event-premium',$post->ID));

	//
	//var_dump($user);
	//if you get here then it's your post type so do your thing....
	}



	add_filter('wp_nav_menu_items', 'add_login_logout_link', 10, 2);
	function add_login_logout_link($items, $args) {
	ob_start();
	ls_loginout('/');
	$loginoutlink = ob_get_contents(); 
	ob_end_clean();
	$items .= '<li>'. $loginoutlink .'</li>';
	return $items; }



	/**
	* Display the Log In/Out link.
	*
	* Displays a link, which allows users to navigate to the Log In page to log in
	* or log out depending on whether they are currently logged in.
	*
	* @since 1.5.0
	*
	* @param string $redirect Optional path to redirect to on login/logout.
	* @param bool   $echo     Default to echo and not return the link.
	* @return string|void String when retrieving.
	*/
	function ls_loginout($redirect = '', $echo = true) {
	global $current_user;
	get_currentuserinfo();
	if ( ! is_user_logged_in() )
	$link = '<a href="' . esc_url( ls_login_url() ) . '">' . __('Log in') . '</a>';
	else{
	$link = '<a href="' . esc_url( ls_logout_url($redirect) ) . '">' . ($current_user->user_login) . '</a>';

	$link .= '<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-522"><a   style="padding:1px;"  title="Dashboard" href="'.  site_url('english-dashboard/').'">Dashboard</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-522"><a style="padding:1px;" title="Logout" href="' . esc_url( ls_logout_url($redirect) ) . '">Logout</a></li>
	</ul>';
	}

	if ( $echo ) {
	/**
	* Filters the HTML output for the Log In/Log Out link.
	*
	* @since 1.5.0
	*
	* @param string $link The HTML link content.
	*/
	echo apply_filters( 'loginout', $link );
	} else {
	/** This filter is documented in wp-includes/general-template.php */
	return apply_filters( 'loginout', $link );
	}
	}

	/**
	* Retrieves the logout URL.
	*
	* Returns the URL that allows the user to log out of the site.
	*
	* @since 2.7.0
	*
	* @param string $redirect Path to redirect to on logout.
	* @return string The logout URL. Note: HTML-encoded via esc_html() in wp_nonce_url().
	*/
	function ls_logout_url($redirect = '') {
	$args = array( 'action' => 'logout' );
	if ( !empty($redirect) ) {
	$args['redirect_to'] = urlencode( $redirect );
	}

	$logout_url = add_query_arg($args, site_url('wp-login.php', 'login'));
	$logout_url = wp_nonce_url( $logout_url, 'log-out' );

	/**
	* Filters the logout URL.
	*
	* @since 2.8.0
	*
	* @param string $logout_url The HTML-encoded logout URL.
	* @param string $redirect   Path to redirect to on logout.
	*/
	return apply_filters( 'logout_url', $logout_url, $redirect );
	}

	/**
	* Retrieves the login URL.
	*
	* @since 2.7.0
	*
	* @param string $redirect     Path to redirect to on log in.
	* @param bool   $force_reauth Whether to force reauthorization, even if a cookie is present.
	*                             Default false.
	* @return string The login URL. Not HTML-encoded.
	*/
	function ls_login_url($redirect = '', $force_reauth = false) {
	$login_url = site_url('english-login/', 'login');

	if ( !empty($redirect) )
	$login_url = add_query_arg('redirect_to', urlencode($redirect), $login_url);

	if ( $force_reauth )
	$login_url = add_query_arg('reauth', '1', $login_url);

	/**
	* Filters the login URL.
	*
	* @since 2.8.0
	* @since 4.2.0 The `$force_reauth` parameter was added.
	*
	* @param string $login_url    The login URL. Not HTML-encoded.
	* @param string $redirect     The path to redirect to on login, if supplied.
	* @param bool   $force_reauth Whether to force reauthorization, even if a cookie is present.
	*/
	return apply_filters( 'login_url', $login_url, $redirect, $force_reauth );
	}

	add_filter('show_admin_bar', '__return_false');

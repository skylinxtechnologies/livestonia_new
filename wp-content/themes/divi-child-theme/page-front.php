<?php

/*
* Template Name: Front Page
*/

?>
<?php get_header(); ?>

<div class="container-fluid" id="fp-container">
	<div class="row">
		<div class="col-sm-3">
			<a href="<?php echo dc_link_of_posts_by_tag('blog-premium'); ?>">
				<section class="post-section" style="background: url(<?php echo dc_thumbnail_posts_by_tag('blog-premium'); ?>); background-size:cover; background-repeat:no-repeat;">
					<p class="section-title">Blog</p>
					<div class="post-group">
					<?php dc_posts_by_tag('blog-premium'); ?>
					</div>
				</section>
			</a>
		</div>
		<div class="col-sm-6">
			<a href="<?php echo dc_link_of_posts_by_tag('event-premium'); ?>">
				<section class="post-section" style="background: url(<?php echo dc_thumbnail_posts_by_tag('event-premium'); ?>); background-size:cover; background-repeat:no-repeat;">
					<p class="section-title">Event</p>
					<div class="post-group">
						<?php dc_posts_by_tag('event-premium'); ?>
					</div>
				</section>
			</a>
		</div>
		<div class="col-sm-3" id="front-sidebar-right">
			<?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('front-sidebar-right')): else: ?>
				<div class="pre-widget">
					<p><strong>Front Right Widget</strong></p>
					<p>This panel is active and ready for you to add some widgets via WordPress admin</p>
				</div>
			<?php endif;?>
		</div> <!-- /#front-sidebar-right -->
	</div> <!-- /.row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="subs-container">
                <?php es_subbox( $namefield = "NO", $desc = "Signup for the best stories of the week:", $group = "" ); ?>
            </div>
		</div><!-- /.col-sm-12 -->
	</div> <!-- /.row -->
	<div class="row">
		<div class="col-sm-6">
			<a href="<?php echo dc_link_of_posts_by_tag('tv-premium'); ?>">
				<section class="post-section" style="background: url(<?php echo dc_thumbnail_posts_by_tag('tv-premium'); ?>); background-size:cover; background-repeat:no-repeat;">
					<p class="section-title">Tv</p>
					<div class="post-group">
					<?php dc_posts_by_tag('tv-premium'); ?>
					</div>
				</section> <!-- /.post-section -->
			</a>
		</div> <!-- /.col-sm-6 -->
		<div class="col-sm-6">
			<a href="<?php echo dc_link_of_posts_by_tag('radio-premium'); ?>">
				<section class="post-section" style="background: url(<?php echo dc_thumbnail_posts_by_tag('radio-premium'); ?>); background-size:cover; background-repeat:no-repeat;">
					<p class="section-title">Radio</p>
					<div class="post-group">
						<?php dc_posts_by_tag('radio-premium'); ?>
					</div>
				</section> <!-- /.post-section -->
			</a>
		</div> <!-- /.col-sm-6 -->
	</div> <!-- /.row -->
	<div class="row blog-banner">
		<div class="col-sm-12">
			<h2 class="banner-title">Blogs</h2>
		</div>
	</div> <!-- /.blog-banner -->
	<div class="row">
		<?php dc_show_latest_blog_posts(); ?>
	</div> <!-- /.row -->
</div><!-- .container-fluid -->

<?php get_footer(); ?>
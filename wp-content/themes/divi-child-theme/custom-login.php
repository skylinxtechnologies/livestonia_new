<?php 
/**
* Template Name: Custom Login
* Description : login page
*/




if(isset($_POST['login_Sbumit'])) {
	$creds                  = array();
	$creds['user_login']    = stripslashes( trim( $_POST['userName'] ) );
	$creds['user_password'] = stripslashes( trim( $_POST['passWord'] ) );
	$creds['remember']      = isset( $_POST['rememberMe'] ) ? sanitize_text_field( $_POST['rememberMe'] ) : '';
	$redirect_to            = esc_url_raw( $_POST['redirect_to'] );
	$secure_cookie          = null;

	if($redirect_to == '')
		$redirect_to= get_site_url(). '/dashboard/' ; 

	if ( ! force_ssl_admin() ) {
		$user = is_email( $creds['user_login'] ) ? get_user_by( 'email', $creds['user_login'] ) : get_user_by( 'login', sanitize_user( $creds['user_login'] ) );

		if ( $user && get_user_option( 'use_ssl', $user->ID ) ) {
			$secure_cookie = true;
			force_ssl_admin( true );
		}
	}

	if ( force_ssl_admin() ) {
		$secure_cookie = true;
	}

	if ( is_null( $secure_cookie ) && force_ssl_login() ) {
		$secure_cookie = false;
	}

	$user = wp_signon( $creds, $secure_cookie );

	if ( $secure_cookie && strstr( $redirect_to, 'wp-admin' ) ) {
		$redirect_to = str_replace( 'http:', 'https:', $redirect_to );
	}

	if ( ! is_wp_error( $user ) ) {
		wp_safe_redirect( $redirect_to );			
	} else {			
		if ( $user->errors ) {
			$errors['invalid_user'] = __('/*<strong>ERROR</strong>: Invalid user or password.*/'); 
		} else {
			$errors['invalid_user_credentials'] = __( 'Please enter your username and password to login.', 'kvcodes' );
		}
	}		 
}
get_header();



?>







<?php if(!empty($errors)) {  //  to print errors,
	foreach($errors as $err )
		echo $err; 
} ?>




<!DOCTYPE html>
<html>
<head>
	<style type="text/css" media="screen">

		@import url(https://fonts.googleapis.com/css?family=Roboto:300);


		.form{
			width: 70%;
			/* display: inline-block; */
			float: left;
		}

		.login-page {
			width: 360px;
			padding: 8% 0 0;
			margin: auto;
		}
/* .form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
} */
.form .message {
	margin: 15px 0 0;
	color: #b3b3b3;
	font-size: 12px;
}
.form .message a {
	color: #4CAF50;
	text-decoration: none;
}
.form .register-form {
	display: none;
}
body {
	background: #cf202c; /* fallback for old browsers */
	background: -webkit-linear-gradient(right, #fff, #fff);
	background: -moz-linear-gradient(right, #fff, #fff);
	background: -o-linear-gradient(right, #fff, #fff);
	background: linear-gradient(to left, #fff, #fff);
	font-family: "Roboto", sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;      
}	
</style>
</head>
<body>
	<div class=container>
		<div class="form">
			<form name="loginform" action="" method="post" style="
			margin-top: 80px;>
			
			<p class="login-username"> 
				<label for="user_login">Username</label>
				<input type="text" name="userName" id="user_login" class="form-control" value="" >
			</p>
			<p class="login-password">
				<label for="user_pass">Password</label>
				<input type="password" name="passWord" id="user_pass" class="form-control" value="" >
			</p>
			
			<p class="login-remember"><label>
				<input name="rememberMe" type="checkbox" id="rememberme" value="forever"> Remember Me</label>
			</p>
			<p class="login-submit">
				<input type="hidden" class="btn btn-success" name="login_Sbumit" >
				<button type="submit" name="wp-submit" id="wp-submit" class="btn btn-danger" value="Log In">Log in</button>
				<input type="hidden" name="redirect_to" class="btn btn-danger"  value="<?php  echo site_url('english-dashboard/')?>">
			</p>
			
		</form>
	</div>
	<?php  
	get_sidebar();

	?>
</div>


</body>
</html>

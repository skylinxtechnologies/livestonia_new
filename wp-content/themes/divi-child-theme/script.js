//divi child script
jQuery(document).ready(function($){
	$('.btn-pencil').click(function(){
		if( $(this).data('click') == 'false'){
			$('.editable-field').attr('contenteditable', 'true').addClass('edit-field');
			$(this).data('click', 'true').text('click here to save');
		}else{
			$('.editable-field').attr('contenteditable', 'false').removeClass('edit-field');
			$(this).data('click', 'false').text('click here to edit');
			var biography = $('.editable-field').text();

			//calling ajax
			$.ajax({
				url:  $('input[name="admin_ajax_url"]').val(),
				data: {action:'update_bio', bio:biography},
				type: 'post',
				beforeSend: function(){
					console.log('Sending data through ajax: '+biography);
				},
				success:function(data, textStatus, jqXHR){
					var response = data.slice(0, -1);
					$('.editable-field').text(response);
				},
				error: function(jqXHR, textStatus, errorThrown){
					console.log('Following error occured: '+ errorThrown);
				}
			});
			
		}
		
	});
});
function validateEmail(field)
{
    var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( !filter.test(field.val()) ){
        field.focus();
        jQuery('.modal-messages').show().html('Please enter valid email address').delay(2000).fadeOut('slow');
        return false;
    }
    else{
        return true;
    }
}

//show custom message popups
function showMessage($message, $status) {
    var old_html = jQuery('.modal-body').html();
    if($status == true)
        jQuery('.modal-body').html($message).css('color', '#5FBA7D');
    else
        jQuery('.modal-body').html($message).css('color', '#CF202C');
    setTimeout(function(){
        jQuery('#followModal').modal('hide');
        jQuery('.modal-body').html(old_html);
    }, 2000);

}
jQuery(document).ready(function($){
    //send data through ajax
    $("#submitFollowingData").click(function(){
        var author_id 		= $('input[name="author_id"]').val();
        var follower_email 	= $('input[name="follower_email"]');
        var ajax_url 		= $('input[name="ajax_url"]').val();

        //validate email
        if( validateEmail(follower_email) !== false){
            var follower_email_value = follower_email.val();
        }else{
            return false;
        }
        $.ajax({
            url:        ajax_url,
            data:       {action:'fa_follow_author', author_id:author_id, follower_email: follower_email_value},
            type:       'post',
            beforeSend: function(){
                console.log('Beginning sending data..');
            },
            success: 	function (data, textStatus, jqXHR) {
                var obj = data.slice(0, -1);
                obj = JSON.parse(obj);
                if(obj.following == true && obj.register == true){
                    showMessage('You have successfully followed the author.', true);
                }else if( obj.following == true){
                    showMessage('You are already following the author.', false);
                }
            },
            error: 	function (jqXHR, textStatus, errorThrown) {
                
            }
        });
    });
});
//subscription form
jQuery(document).ready(function($){
    //console.log($(".es_textbox"));
    $(".es_textbox").append('<span class="glyphicon glyphicon-play"></span>');

    $(".es_textbox span").click(function(){
        $(".es_button input").click();
    });
});
<?php
if(isset($_GET['preview'], $_GET['view'], $_GET['news'], $_GET['p']) && $_GET['view'] == 'newsletter')
{
	$newsletter_id = $_GET['news'];
	$post_id = $_GET['p'];
	
	global $wpdb;
	$table = $wpdb->prefix . 'es_templatetable';
	$results = $wpdb->get_results("SELECT * FROM $table WHERE `es_templ_id` = $newsletter_id");

	
	//process newsletter preview post
	get_header();
	?>
<div class="container" id="newsletter">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="row" id="logo-container">
			<div class="col-sm-8">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/uploads/logo.png" class="logo">
			</div>
			<div class="col-sm-4">
				<p class="logo-text">PRESS NEWS</p>
			</div>
		</div> <!-- .row -->
		<div class="row" id="content">
			<?php
			$the_query = new WP_Query(['p'=>$post_id]);
			if( $the_query->have_posts() )
			{
				while( $the_query->have_posts() )
				{
					$the_query->the_post();
					dc_publish_newsletter_content($results, $the_query);
				}
			}
			?>
			<!--<h1>Article title here</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at felis ullamcorper, imperdiet enim non, facilisis augue. Sed vel ullamcorper ipsum. Vivamus in volutpat arcu. Donec sit amet pellentesque turpis, sit amet consequat turpis. Nulla egestas sollicitudin justo et blandit. Suspendisse pharetra porttitor sem, et dignissim neque ullamcorper non. Aliquam ultricies tellus ut felis rutrum, sit amet sodales eros tristique. Duis ut odio eget mauris suscipit accumsan.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at felis ullamcorper, imperdiet enim non, facilisis augue. Sed vel ullamcorper ipsum. Vivamus in volutpat arcu. Donec sit amet pellentesque turpis, sit amet consequat turpis. Nulla egestas sollicitudin justo et blandit. Suspendisse pharetra porttitor sem, et dignissim neque ullamcorper non. Aliquam ultricies tellus ut felis rutrum, sit amet sodales eros tristique. Duis ut odio eget mauris suscipit accumsan.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at felis ullamcorper, imperdiet enim non, facilisis augue. Sed vel ullamcorper ipsum. Vivamus in volutpat arcu. Donec sit amet pellentesque turpis, sit amet consequat turpis. Nulla egestas sollicitudin justo et blandit. Suspendisse pharetra porttitor sem, et dignissim neque ullamcorper non. Aliquam ultricies tellus ut felis rutrum, sit amet sodales eros tristique. Duis ut odio eget mauris suscipit accumsan.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at felis ullamcorper, imperdiet enim non, facilisis augue. Sed vel ullamcorper ipsum. Vivamus in volutpat arcu. Donec sit amet pellentesque turpis, sit amet consequat turpis. Nulla egestas sollicitudin justo et blandit. Suspendisse pharetra porttitor sem, et dignissim neque ullamcorper non. Aliquam ultricies tellus ut felis rutrum, sit amet sodales eros tristique. Duis ut odio eget mauris suscipit accumsan.</p>
			<p class="author-name">Author Name</p>
			<p class="link"><a href="">Read this on livestonia</a></p>
			<p class="link"><a href="">Click here for photo link</a></p>-->
		</div> <!-- .row-->
	</div> <!-- .col-sm-6 -->
</div> <!-- .container -->
	<?php
	get_footer();
}
else
{
	get_header();
	$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );
	$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
	?>
	<div id="main-content">
		<?php while( have_posts() ): the_post(); ?>
 		<?php if (et_get_option('divi_integration_single_top') <> '' && et_get_option('divi_integrate_singletop_enable') == 'on') echo(et_get_option('divi_integration_single_top')); ?>
 
 		<?php
 			$et_pb_has_comments_module = has_shortcode( get_the_content(), 'et_pb_comments' );
 			$additional_class = $et_pb_has_comments_module ? ' et_pb_no_comments_section' : '';
 		?>
 		<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' . $additional_class ); ?>
 			<div class="container-fluid article-bg-container">
 					<?php if( has_post_thumbnail() ):?>
 						<div class="article-bg" style="background: url('<?php echo get_the_post_thumbnail_url(null, 'full'); ?>'); background-repeat:no-repeat;background-size: cover;">
 							<?php if ( ( 'off' !== $show_default_title && $is_page_builder_used ) || ! $is_page_builder_used ) { ?>
 								<div class="et_post_meta_wrapper">
 									<h1 class="entry-title"><?php the_title(); ?></h1>
 									<p>by <span class="post-author-text"><?php the_author(); ?></span></p>
 									<p>on <span class="post-date"><?php echo get_the_date('F j, Y'); ?></span></p>
 									
 								</div> <!-- .et_post_meta_wrapper -->
 							<?php } ?>
 						</div>
 					<?php else:?>
 							<?php if ( ( 'off' !== $show_default_title && $is_page_builder_used ) || ! $is_page_builder_used ) { ?>
 								<div class="container">
 									<h1 class="entry-title" style="padding-bottom:0;"><?php the_title(); ?></h1>
 									<?php
 									if ( ! post_password_required() ) :
 
 										et_divi_post_meta();
 
 										$thumb = '';
 
 										$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );
 
 										$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
 										$classtext = 'et_featured_image';
 										$titletext = get_the_title();
 										$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
 										$thumb = $thumbnail["thumb"];
 
 										$post_format = et_pb_post_format();
 
 										if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) {
 											printf(
 												'<div class="et_main_video_container">
 													%1$s
 												</div>',
 												$first_video
 											);
 										} else if ( ! in_array( $post_format, array( 'gallery', 'link', 'quote' ) ) && 'on' === et_get_option( 'divi_thumbnails', 'on' ) && '' !== $thumb ) {
 											print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
 										} else if ( 'gallery' === $post_format ) {
 											et_pb_gallery_images();
 										}
 									?>
 
 									<?php
 										$text_color_class = et_divi_get_post_text_color();
 
 										$inline_style = et_divi_get_post_bg_inline_style();
 
 										switch ( $post_format ) {
 											case 'audio' :
 												printf(
 													'<div class="et_audio_content%1$s"%2$s>
 														%3$s
 													</div>',
 													esc_attr( $text_color_class ),
 													$inline_style,
 													et_pb_get_audio_player()
 												);
 
 												break;
 											case 'quote' :
 												printf(
 													'<div class="et_quote_content%2$s"%3$s>
 														%1$s
 													</div> .et_quote_content',
 													et_get_blockquote_in_content(),
 													esc_attr( $text_color_class ),
 													$inline_style
 												);
 
 												break;
 											case 'link' :
 												printf(
 													'<div class="et_link_content%3$s"%4$s>
 														<a href="%1$s" class="et_link_main_url">%2$s</a>
 													</div> .et_link_content',
 													esc_url( et_get_link_url() ),
 													esc_html( et_get_link_url() ),
 													esc_attr( $text_color_class ),
 													$inline_style
 												);
 
 												break;
 										}
 
 									endif;
 									?>
 								</div>
 							<?php } ?>
 					<?php endif; ?>
 			</div>
 			<div class="container" id="content-container">
 				<div id="content-area" class="clearfix">
 					<div id="left-area">
 						<div class="entry-content">
 						<?php
 							do_action( 'et_before_content' );
 
 							the_content();
 
 							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
 						?>
 						</div>
 						<?php
 						if ( et_get_option('divi_468_enable') == 'on' ){
 							echo '<div class="et-single-post-ad">';
 							if ( et_get_option('divi_468_adsense') <> '' ) echo( et_get_option('divi_468_adsense') );
 							else { ?>
 								<a href="<?php echo esc_url(et_get_option('divi_468_url')); ?>"><img src="<?php echo esc_attr(et_get_option('divi_468_image')); ?>" alt="468" class="foursixeight" /></a>
 						<?php 	}
 								echo '</div> .et-single-post-ad';
 							}
 						?>
 						<!-- comment box start -->
 						<?php
 							if ( ( comments_open() || get_comments_number() ) && 'on' == et_get_option( 'divi_show_postcomments', 'on' ) && ! $et_pb_has_comments_module ) {
 								comments_template( '', true );
 							}
 						?>
 						<!-- comment box end -->
 					</div>
 					<?php if (et_get_option('divi_integration_single_bottom') <> '' && et_get_option('divi_integrate_singlebottom_enable') == 'on') echo(et_get_option('divi_integration_single_bottom')); ?>
 					<?php get_sidebar(); ?>
 				</div>
 			</div>
 		</article>
 	<?php endwhile; ?>
	</div> <!-- #main-content -->
	<script>
		jQuery(document).ready(function($){
			$('.reply-container a').removeClass();
		});
	</script>
	<?php
	get_footer();
}
?>
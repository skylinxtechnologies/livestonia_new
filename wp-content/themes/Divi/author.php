<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
			
	
	<link rel="pingback" href="http://livestonia.ee/xmlrpc.php" />

		<!--[if lt IE 9]>
	<script src="http://livestonia.ee/wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<title>Hello World! | Live Estonia</title>
<link rel="alternate" type="application/rss+xml" title="Live Estonia &raquo; Feed" href="http://livestonia.ee/feed/" />
<link rel="alternate" type="application/rss+xml" title="Live Estonia &raquo; Comments Feed" href="http://livestonia.ee/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Live Estonia &raquo; Hello World! Comments Feed" href="http://livestonia.ee/2016/05/12/test/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/livestonia.ee\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.3"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<meta content="Divi v.2.7" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='es-widget-css-css'  href='http://livestonia.ee/wp-content/plugins/email-subscribers/widget/es-widget.css?ver=4.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='events-manager-css'  href='http://livestonia.ee/wp-content/plugins/events-manager/includes/css/events_manager.css?ver=5.64' type='text/css' media='all' />
<link rel='stylesheet' id='facebook-login-css'  href='http://livestonia.ee/wp-content/plugins/wp-facebook-login/public/css/facebook-login.css?ver=1.1.4' type='text/css' media='all' />
<link rel='stylesheet' id='divi-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='divi-style-css'  href='http://livestonia.ee/wp-content/themes/Divi/style.css?ver=2.7' type='text/css' media='all' />
<link rel='stylesheet' id='et-shortcodes-css-css'  href='http://livestonia.ee/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes.css?ver=2.7' type='text/css' media='all' />
<link rel='stylesheet' id='et-shortcodes-responsive-css-css'  href='http://livestonia.ee/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes_responsive.css?ver=2.7' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-css'  href='http://livestonia.ee/wp-content/themes/Divi/includes/builder/styles/magnific_popup.css?ver=2.7' type='text/css' media='all' />
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/sortable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/menu.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/wp-a11y.min.js?ver=4.5.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var uiAutocompleteL10n = {"noResults":"No search results.","oneResult":"1 result found. Use up and down arrow keys to navigate.","manyResults":"%d results found. Use up and down arrow keys to navigate."};
/* ]]> */
</script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/resizable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/draggable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/button.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://livestonia.ee/wp-includes/js/jquery/ui/dialog.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var EM = {"ajaxurl":"http:\/\/livestonia.ee\/wp-admin\/admin-ajax.php","locationajaxurl":"http:\/\/livestonia.ee\/wp-admin\/admin-ajax.php?action=locations_search","firstDay":"1","locale":"en","dateFormat":"dd\/mm\/yy","ui_css":"http:\/\/livestonia.ee\/wp-content\/plugins\/events-manager\/includes\/css\/jquery-ui.min.css","show24hours":"","is_ssl":"","bookingInProgress":"Please wait while the booking is being submitted.","tickets_save":"Save Ticket","bookingajaxurl":"http:\/\/livestonia.ee\/wp-admin\/admin-ajax.php","bookings_export_save":"Export Bookings","bookings_settings_save":"Save Settings","booking_delete":"Are you sure you want to delete?","bb_full":"Sold Out","bb_book":"Book Now","bb_booking":"Booking...","bb_booked":"Booking Submitted","bb_error":"Booking Error. Try again?","bb_cancel":"Cancel","bb_canceling":"Canceling...","bb_cancelled":"Cancelled","bb_cancel_error":"Cancellation Error. Try again?","txt_search":"Search","txt_searching":"Searching...","txt_loading":"Loading..."};
/* ]]> */
</script>
<script type='text/javascript' src='http://livestonia.ee/wp-content/plugins/events-manager/includes/js/events-manager.js?ver=5.64'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fbl = {"ajaxurl":"http:\/\/livestonia.ee\/wp-admin\/admin-ajax.php","site_url":"http:\/\/livestonia.ee","scopes":"email,public_profile","appId":"1587114521618605","l18n":{"chrome_ios_alert":"Please login into facebook and then click connect button again"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://livestonia.ee/wp-content/plugins/wp-facebook-login/public/js/facebook-login.js?ver=1.1.4'></script>
<link rel='https://api.w.org/' href='http://livestonia.ee/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://livestonia.ee/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://livestonia.ee/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.5.3" />
<link rel="canonical" href="http://livestonia.ee/2016/05/12/test/" />
<link rel='shortlink' href='http://livestonia.ee/?p=102' />
<link rel="alternate" type="application/json+oembed" href="http://livestonia.ee/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flivestonia.ee%2F2016%2F05%2F12%2Ftest%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://livestonia.ee/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flivestonia.ee%2F2016%2F05%2F12%2Ftest%2F&#038;format=xml" />
<style type="text/css">
.qtranxs_flag_ru {background-image: url(http://livestonia.ee/wp-content/plugins/qtranslate-x/flags/ru.png); background-repeat: no-repeat;}
.qtranxs_flag_et {background-image: url(http://livestonia.ee/wp-content/plugins/qtranslate-x/flags/ee.png); background-repeat: no-repeat;}
.qtranxs_flag_en {background-image: url(http://livestonia.ee/wp-content/plugins/qtranslate-x/flags/gb.png); background-repeat: no-repeat;}
</style>
<link hreflang="ru" href="http://livestonia.ee/ru/2016/05/12/test/" rel="alternate" />
<link hreflang="et" href="http://livestonia.ee/et/2016/05/12/test/" rel="alternate" />
<link hreflang="en" href="http://livestonia.ee/en/2016/05/12/test/" rel="alternate" />
<link hreflang="x-default" href="http://livestonia.ee/2016/05/12/test/" rel="alternate" />
<meta name="generator" content="qTranslate-X 3.4.6.8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />		<style id="theme-customizer-css">
													.woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce-message, .woocommerce-error, .woocommerce-info { background: #ffffff !important; }
			#et_search_icon:hover, .mobile_menu_bar:before, .mobile_menu_bar:after, .et_toggle_slide_menu:after, .et-social-icon a:hover, .et_pb_sum, .et_pb_pricing li a, .et_pb_pricing_table_button, .et_overlay:before, .entry-summary p.price ins, .woocommerce div.product span.price, .woocommerce-page div.product span.price, .woocommerce #content div.product span.price, .woocommerce-page #content div.product span.price, .woocommerce div.product p.price, .woocommerce-page div.product p.price, .woocommerce #content div.product p.price, .woocommerce-page #content div.product p.price, .et_pb_member_social_links a:hover, .woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before, .et_pb_widget li a:hover, .et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active, .et_pb_filterable_portfolio .et_pb_portofolio_pagination ul li a.active, .et_pb_gallery .et_pb_gallery_pagination ul li a.active, .wp-pagenavi span.current, .wp-pagenavi a:hover, .nav-single a, .posted_in a { color: #ffffff; }
			.et_pb_contact_submit, .et_password_protected_form .et_submit_button, .et_pb_bg_layout_light .et_pb_newsletter_button, .comment-reply-link, .form-submit input, .et_pb_bg_layout_light .et_pb_promo_button, .et_pb_bg_layout_light .et_pb_more_button, .woocommerce a.button.alt, .woocommerce-page a.button.alt, .woocommerce button.button.alt, .woocommerce-page button.button.alt, .woocommerce input.button.alt, .woocommerce-page input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce-page #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page #content input.button.alt, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button { color: #ffffff; }
			.footer-widget h4 { color: #ffffff; }
			.et-search-form, .nav li ul, .et_mobile_menu, .footer-widget li:before, .et_pb_pricing li:before, blockquote { border-color: #ffffff; }
			.et_pb_counter_amount, .et_pb_featured_table .et_pb_pricing_heading, .et_quote_content, .et_link_content, .et_audio_content, .et_pb_post_slider.et_pb_bg_layout_dark, .et_slide_in_menu_container { background-color: #ffffff; }
							.container, .et_pb_row, .et_pb_slider .et_pb_container, .et_pb_fullwidth_section .et_pb_title_container, .et_pb_fullwidth_section .et_pb_title_featured_container, .et_pb_fullwidth_header:not(.et_pb_fullscreen) .et_pb_fullwidth_header_container { max-width: 960px; }
			.et_boxed_layout #page-container, .et_fixed_nav.et_boxed_layout #page-container #top-header, .et_fixed_nav.et_boxed_layout #page-container #main-header, .et_boxed_layout #page-container .container, .et_boxed_layout #page-container .et_pb_row { max-width: 1120px; }
							a { color: #666666; }
							#main-header, #main-header .nav li ul, .et-search-form, #main-header .et_mobile_menu { background-color: #cf202c; }
											#top-header, #et-secondary-nav li ul { background-color: #ffffff; }
													.et_header_style_centered .mobile_nav .select_page, .et_header_style_split .mobile_nav .select_page, .et_nav_text_color_light #top-menu > li > a, .et_nav_text_color_dark #top-menu > li > a, #top-menu a, .et_mobile_menu li a, .et_nav_text_color_light .et_mobile_menu li a, .et_nav_text_color_dark .et_mobile_menu li a, #et_search_icon:before, .et_search_form_container input, span.et_close_search_field:after, #et-top-navigation .et-cart-info { color: #ffffff; }
			.et_search_form_container input::-moz-placeholder { color: #ffffff; }
			.et_search_form_container input::-webkit-input-placeholder { color: #ffffff; }
			.et_search_form_container input:-ms-input-placeholder { color: #ffffff; }
											#top-menu li a { font-size: 15px; }
			body.et_vertical_nav .container.et_search_form_container .et-search-form input { font-size: 15px !important; }
		
		
					#top-menu li.current-menu-ancestor > a, #top-menu li.current-menu-item > a,
			.et_color_scheme_red #top-menu li.current-menu-ancestor > a, .et_color_scheme_red #top-menu li.current-menu-item > a,
			.et_color_scheme_pink #top-menu li.current-menu-ancestor > a, .et_color_scheme_pink #top-menu li.current-menu-item > a,
			.et_color_scheme_orange #top-menu li.current-menu-ancestor > a, .et_color_scheme_orange #top-menu li.current-menu-item > a,
			.et_color_scheme_green #top-menu li.current-menu-ancestor > a, .et_color_scheme_green #top-menu li.current-menu-item > a { color: #ffffff; }
													#main-footer .footer-widget h4 { color: #ffffff; }
							.footer-widget li:before { border-color: #ffffff; }
																
		
																														
		@media only screen and ( min-width: 981px ) {
							.et_pb_section { padding: 2% 0; }
				.et_pb_section.et_pb_section_first { padding-top: inherit; }
				.et_pb_fullwidth_section { padding: 0; }
										.et_pb_row { padding: 0% 0; }
																.et_header_style_left #et-top-navigation, .et_header_style_split #et-top-navigation  { padding: 15px 0 0 0; }
				.et_header_style_left #et-top-navigation nav > ul > li > a, .et_header_style_split #et-top-navigation nav > ul > li > a { padding-bottom: 15px; }
				.et_header_style_split .centered-inline-logo-wrap { width: 30px; margin: -30px 0; }
				.et_header_style_split .centered-inline-logo-wrap #logo { max-height: 30px; }
				.et_pb_svg_logo.et_header_style_split .centered-inline-logo-wrap #logo { height: 30px; }
				.et_header_style_centered #top-menu > li > a { padding-bottom: 5px; }
				.et_header_style_slide #et-top-navigation, .et_header_style_fullscreen #et-top-navigation { padding: 6px 0 6px 0 !important; }
									.et_header_style_centered #main-header .logo_container { height: 30px; }
														#logo { max-height: 81%; }
				.et_pb_svg_logo #logo { height: 81%; }
																.et_header_style_centered.et_hide_primary_logo #main-header:not(.et-fixed-header) .logo_container, .et_header_style_centered.et_hide_fixed_logo #main-header.et-fixed-header .logo_container { height: 5.4px; }
																.et-fixed-header#top-header, .et-fixed-header#top-header #et-secondary-nav li ul { background-color: #ffffff; }
													.et-fixed-header #top-menu li a { font-size: 15px; }
										.et-fixed-header #top-menu a, .et-fixed-header #et_search_icon:before, .et-fixed-header #et_top_search .et-search-form input, .et-fixed-header .et_search_form_container input, .et-fixed-header .et_close_search_field:after, .et-fixed-header #et-top-navigation .et-cart-info { color: #ffffff !important; }
				.et-fixed-header .et_search_form_container input::-moz-placeholder { color: #ffffff !important; }
				.et-fixed-header .et_search_form_container input::-webkit-input-placeholder { color: #ffffff !important; }
				.et-fixed-header .et_search_form_container input:-ms-input-placeholder { color: #ffffff !important; }
										.et-fixed-header #top-menu li.current-menu-ancestor > a,
				.et-fixed-header #top-menu li.current-menu-item > a { color: #ffffff !important; }
						
					}
		@media only screen and ( min-width: 1200px) {
			.et_pb_row { padding: 0px 0; }
			.et_pb_section { padding: 24px 0; }
			.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper { padding-top: 0px; }
			.et_pb_section.et_pb_section_first { padding-top: inherit; }
			.et_pb_fullwidth_section { padding: 0; }
		}
		@media only screen and ( max-width: 980px ) {
																				}
		@media only screen and ( max-width: 767px ) {
														}
	</style>

	
	<style id="module-customizer-css">
			</style>

			<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<style type="text/css" id="et-custom-css">
.et_pb_fullwidth_post_title_10000 {
    background-image: url("http://livestonia.ee/wp-content/uploads/2016/05/vaade_olevistest_kadi-liis_koppel_2015_5.jpg");
}

#page-container {
padding-top:0 !important}
</style></head>
<body class="single single-post single-format-standard et_pb_button_helper_class et_fullwidth_nav et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters1 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_full_width_page gecko">
	<div id="page-container">
	
	
		<header id="main-header" data-height-onload="30">
			<div class="container clearfix et_menu_container">
							<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="http://livestonia.ee/">
						<img src="http://livestonia.ee/wp-content/uploads/2016/07/fAsfGgR.png" alt="Live Estonia" id="logo" data-height-percentage="81" />
					</a>
				</div>
				<div id="et-top-navigation" data-height="30" data-fixed-height="40">
											<nav id="top-menu-nav">
						<ul id="top-menu" class="nav"><li id="menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-41"><a href="/about">About</a></li>
<li id="menu-item-42" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-42"><a href="/groups">Groups</a></li>
<li id="menu-item-77" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-77"><a href="http://livestonia.ee/wp-login.php?action=register">Log In</a></li>
</ul>						</nav>
									
					
					
					<div id="et_mobile_nav_menu">
				<div class="mobile_nav closed">
					<span class="select_page">Select Page</span>
					<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
				</div>
			</div>				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="http://livestonia.ee/">
					<input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header> 

<div id="et-main-area">
<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">


<!-- This sets the $curauth variable -->

    <?php
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    ?>

<article id="post-107" class="post-107 page type-page status-publish hentry">

				
					<div class="entry-content">
					<div class="et_pb_section et_pb_fullwidth_section  et_pb_section_0 et_section_regular">
				
				
<div class="et_pb_module et_pb_post_title   et_pb_fullwidth_post_title_10000 et_pb_bg_layout_dark et_pb_featured_bg">
				
				
				<div class="et_pb_title_container">
					<h1><?php echo get_avatar($curauth->ID) . "<br>" . $curauth->first_name . " " . $curauth->last_name; ?></h1>
				</div>
					</div>
					<div class=" et_pb_row et_pb_row_0">
				
				<div class="et_pb_column et_pb_column_4_4  et_pb_column_0">
				
				<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_0">
				 
			</div> <!-- .et_pb_text --><div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_1">
				
<dl>
        <dt>Website</dt>
        <dd><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></dd>
        <dt>Profile</dt>
        <dd><?php echo $curauth->user_description; ?></dd>
    </dl>

    <h2>Posts by <?php echo $curauth->nickname; ?>:</h2>

    <ul>
<!-- The Loop -->

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <li>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
            <?php the_title(); ?></a>,
            <?php the_time('d M Y'); ?> in <?php the_category('&');?>
        </li>

    <?php endwhile; else: ?>
        <p><?php _e('No posts by this author.'); ?></p>

    <?php endif; ?>

<!-- End Loop -->

    </ul>

			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
					
			</div> <!-- .et_pb_row -->
				
			</div> <!-- .et_pb_section -->
					</div> <!-- .entry-content -->

				
				</article> <!-- .et_pb_post -->

   


</div></div></div></div></div>

<footer id="main-footer">
				

		
				<div id="footer-bottom">
					<div class="container clearfix">
				<ul class="et-social-icons">


</ul>
						<p id="footer-info"> <a href="http://http://www.livestonia.ee" title="Livestonia.ee">Livestonia.ee</a> </p>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->


	</div> <!-- #page-container -->

	
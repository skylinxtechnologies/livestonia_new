<?php

	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 1001);
	function theme_enqueue_styles() {
	    wp_enqueue_style('td-theme', get_template_directory_uri() . '/style.css', '', TD_THEME_VERSION . 'c' , 'all' );
	    wp_enqueue_style('td-theme-child', get_stylesheet_directory_uri() . '/style.css', array('td-theme'), TD_THEME_VERSION . 'c', 'all' );

	}

/*  Custom Post Types */

function post_type_video() {
		$labels = array(
	    	'name' => _x('Videos', 'post type general name', 'agrg'),
	    	'singular_name' => _x('videos', 'post type singular name', 'agrg'),
	    	'add_new' => _x('Add New Video', 'video', 'agrg'),
	    	'add_new_item' => __('Add New Video', 'agrg'),
	    	'edit_item' => __('Edit Video', 'agrg'),
	    	'new_item' => __('New Video', 'agrg'),
	    	'view_item' => __('View Video', 'agrg'),
	    	'search_items' => __('Search Videos', 'agrg'),
	    	'not_found' =>  __('No Video found', 'agrg'),
	    	'not_found_in_trash' => __('No Video found in Trash', 'agrg'),
	    	'parent_item_colon' => ''
		);
		$args = array(
	    	'labels' => $labels,
	    	'public' => true,
	    	'publicly_queryable' => true,
	    	'show_ui' => true,
	    	'query_var' => true,
	    	'capability_type' => 'post',
	    	'hierarchical' => true,
	    	'menu_position' => null,
	    	'has_archive' => true,
	    	'menu_icon' => 'dashicons-welcome-learn-more',
	    	'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'page-attributes', 'post-formats' )
		);
		register_post_type( 'video', $args );
		flush_rewrite_rules();
	}
	add_action('init', 'post_type_video');

	function create_video_taxonomies() {
		$labels = array(
			'name'              => _x( 'Video Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Video Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Video Categories' ),
			'all_items'         => __( 'All Video Categories' ),
			'parent_item'       => __( 'Parent Video Category' ),
			'parent_item_colon' => __( 'Parent Video Category:' ),
			'edit_item'         => __( 'Edit Video Category' ),
			'update_item'       => __( 'Update Video Category' ),
			'add_new_item'      => __( 'Add New Video Category' ),
			'new_item_name'     => __( 'New Video Category Name' ),
			'menu_name'         => __( 'Video Categories' ),
		);
		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'videos' ),
		);
		register_taxonomy( 'video_categories', array( 'video' ), $args );
	}
	add_action( 'init', 'create_video_taxonomies', 0 );
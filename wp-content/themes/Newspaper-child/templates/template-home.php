<?php 

/*
*
* Template Name: Home Page
*
*/

get_header();  ?>


<div class="container">
	
	<div class="row">
		<div class="col-md-8 padding post lg-video">
			<?php 

				global $wpdb;

				$query = "SELECT wp_posts.ID FROM wp_posts LEFT JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) WHERE 1=1 AND ( wp_term_relationships.term_taxonomy_id IN (52) ) AND wp_posts.post_type = 'video' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private') GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 10";


				$results = $wpdb->get_results($query);

				foreach ($results as $value) { ?>
					
					<div class="inner">

						<a href="<? echo the_permalink($value->ID); ?>">
							<div class="background">

								<?php 
									$post_img  = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
								?>

								<img src="<?php echo $post_img; ?>" width="789px" height="443px"  />

							</div>
							<div class="opacity-layer"></div>

							<div class="preview">

								<div class="play-icon"></div>

								<h2>
									<?php echo get_the_title($value->ID); ?>
									<span class="author">
										<span><?php echo the_author($value->ID);?></span>
										<div class="author-icon">
											<div class="icon-container">
												<div class="icon follow"></div>
												<div class="tooltip">subscribe</div>
											</div>
										</div>
									</span>
								</h2>

							</div>

						</a>

					</div>	

					<?php 

				}

			?> 
		</div>
		<div class="col-md-2 padding post event">
			<?php 	
				$query = "SELECT wp_posts.ID FROM wp_posts WHERE 1=1 AND wp_posts.post_type = 'event' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private') ORDER BY wp_posts.post_date DESC LIMIT 0, 1";
					$results = $wpdb->get_results($query);

					foreach ($results as $value) { ?>
					<div class="inner">
						<section>
							<div class="author">
								<div class="helper-author">
									<span class="userpic">
										<a href="#">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/userimg.jpeg">
										</a>
									</span>
									<span class="username">
										<a href="#">
											<span class="firstName">Katerina</span><br>
											<span class="lastName">Romanenko</span>
										</a>
									</span>
								</div>
							</div>
							<a href="<? echo the_permalink($value->ID); ?>">
								<h2><?php echo get_the_title($value->ID); ?></h2>
							</a>
							<div class="image-wrapper">
								<?php 
									$post_img  = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
								?>
								<div class="image" style="background-image: url(<?php echo $post_img; ?>);">
								</div>
							</div>
							<!-- react-empty: 13379 -->
						</section>
					</div>
			<?php }?> 
		</div>
		<div class="col-md-2 padding post sm-video">
			<?php

				$query = "SELECT wp_posts.ID FROM wp_posts WHERE 1=1 AND wp_posts.post_type = 'video' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private') ORDER BY wp_posts.post_date DESC LIMIT 1, 1";

				$results = $wpdb->get_results($query);

				foreach ($results as $value) { ?>

					<div class="inner">

						<a href="<? echo the_permalink($value->ID); ?>">

							<div class="background">
								
								<?php 
									$post_img  = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
								?>

								<div class="img" style="background-image: url(<?php echo $post_img; ?>);"></div>

							</div>

							<div class="opacity-layer"></div>
							<div class="play-icon"></div>
							<p><?php echo get_the_title($value->ID); ?></p>
						</a>

					</div>

					<?php 

				}

			?> 
		</div>

	</div>
	<div class="row">
		<div class="col-md-10">
			<div class="row">
				<?php 

					global $wpdb;

					$query = "SELECT wp_posts.ID FROM wp_posts WHERE 1=1 AND wp_posts.post_type = 'post' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private') GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 5";


					$results = $wpdb->get_results($query);

					foreach ($results as $value) { ?>
						<div class="col-md-6 padding md-post">
						
							<div class="inner">
								<a href="<? echo the_permalink($value->ID); ?>">
									<div class="background">

										<?php 
											$post_img  = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
										?>

										<img src="<?php echo $post_img; ?>" width="789px" height="443px"  />

									</div>
									<div class="shadow"></div>

									<div class="preview">
										<h2>
											<?php echo get_the_title($value->ID); ?>
											<span class="author">
												<span>Kseniya CHurmanteeva</span>
												<div class="component-follow-author-icon">
													<div class="icon-container">
														<div class="icon follow"></div>
														<div class="tooltip">subscribe</div>
													</div>
												</div>
											</span>
										</h2>
										<a href="<? echo the_permalink($value->ID); ?>">
											<p><?php echo the_excerpt($value->ID); ?></p>
										</a>

									</div>

								</a>

							</div>	
						</div>
				<?php } ?> 
			</div>
		</div>
		<?php 	
			$query = "SELECT wp_posts.ID FROM wp_posts WHERE 1=1 AND wp_posts.post_type = 'event' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private') ORDER BY wp_posts.post_date DESC LIMIT 0, 3";
				$results = $wpdb->get_results($query);

				foreach ($results as $value) { ?>
				<div class="col-md-2 padding event post">
					<div class="inner">
						<section>
							<div class="author">
								<div class="helper-author">
									<span class="userpic">
										<a href="#">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/userimg.jpeg">
										</a>
									</span>
									<span class="username">
										<a href="#">
											<span class="firstName">Katerina</span><br>
											<span class="lastName">Romanenko</span>
										</a>
									</span>
								</div>
							</div>
							<a href="<? echo the_permalink($value->ID); ?>">
								<h2><?php echo get_the_title($value->ID); ?></h2>
							</a>
							<div class="image-wrapper">
								<?php 
									$post_img  = wp_get_attachment_url( get_post_thumbnail_id($value->ID) );
								?>
								<div class="image" style="background-image: url(<?php echo $post_img; ?>);">
								</div>
							</div>
							<!-- react-empty: 13379 -->
						</section>
					</div>
				</div>
		<?php }?> 
	</div>

</div>




<?php get_footer();  ?>
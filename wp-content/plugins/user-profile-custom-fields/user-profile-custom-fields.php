<?php
/**
* Plugin Name: 	User Profile Custom Fields
* Plugin URI:  	https://drive.google.com/open?id=0B3RmFEM0zy_2ZzdDeG4ycW5nV1E
* Description: 	This plugin creates and shows custom fields in user profile i.e. youtube video, user bio
* Version:		1.0
* Author:		Muhammad Waseem
* Author URI:	https://www.facebook.com/engineer.m.waseem
**/

add_action('show_user_profile', 'upcf_show_user_fields');
add_action('edit_user_profile', 'upcf_show_user_fields');

function upcf_show_user_fields($user)
{
?>
	<h3>Author Page Extra Fields</h3>
	<table class="form-table">
		
		<tr>
			<th><label for="phone_number">Phone Number</label></th>
			<td>
				<input type="text" name="phone_number" id="phone_number" class="regular-text" value="<?php echo esc_attr( get_the_author_meta('phone_number', $user->ID) );?>" >
				<span class="description">Please enter your Phone number.</span>
			</td>
		</tr>
		<tr>
			<th><label for="address">Address</label></th>
			<td>
				<textarea rows="5" cols="30" name="address" id="address" class="regular-text" ><?php echo esc_attr( get_the_author_meta('address', $user->ID) );?></textarea>
				<span class="description">Please enter your Address.</span>
			</td>
		</tr>
		<tr>
			<th><label for="youtube_video_url">Youtube Video URL</label></th>
			<td>
				<input type="text" name="youtube_video_url" id="youtube_video_url" class="regular-text" value="<?php echo esc_attr( get_the_author_meta('youtube_video_url', $user->ID) );?>" >
				<span class="description">Please enter your Youtube Video URL.</span>
			</td>
		</tr>
		<tr>
			<th><label for="facebook_username">Facebook Username</label></th>
			<td>
				<input type="text" name="facebook_username" id="facebook_username" class="regular-text" value="<?php echo esc_attr( get_the_author_meta('facebook_username', $user->ID) );?>" >
				<span class="description">Please enter your Facebook username.</span>
			</td>
		</tr>
		<tr>
			<th><label for="skype_username">Skype Username</label></th>
			<td>
				<input type="text" name="skype_username" id="skype_username" class="regular-text" value="<?php echo esc_attr( get_the_author_meta('skype_username', $user->ID) );?>" >
				<span class="description">Please enter your Skype username.</span>
			</td>
		</tr>
	</table>
<?php
}

add_action('personal_options_update', 'upcf_save_user_fields');
add_action('edit_user_profile_update', 'upcf_save_user_fields');

function upcf_save_user_fields($user_id)
{
	update_user_meta($user_id, 'youtube_video_url', sanitize_text_field( $_POST['youtube_video_url'] ));
	update_user_meta($user_id, 'phone_number', sanitize_text_field( $_POST['phone_number'] ));
	update_user_meta($user_id, 'address', sanitize_text_field( $_POST['address'] ));
	update_user_meta($user_id, 'skype_username', sanitize_text_field( $_POST['skype_username'] ));
	update_user_meta($user_id, 'facebook_username', sanitize_text_field( $_POST['facebook_username'] ));
}
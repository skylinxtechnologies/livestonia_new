<?php
/*
 * FA_Ajax handles ajax process for subscription
 * @since 1.0
 */
class FA_Ajax
{

    /*
     * @var integer $max_id the maximum number used as id
     */
    static private $max_id = 9999;
    /*
     * Autoload the ajax handlers
     * @since 1.0
     */
    public function __construct()
    {
        add_action('wp_ajax_nopriv_fa_follow_author', array(__CLASS__, 'followAuthorHandle'));
        add_action('wp_ajax_fa_follow_author', array(__CLASS__, 'followAuthorHandle'));
    }
    
    /*
     * @since 1.0
     */
    public static function followAuthorHandle()
    {
        global $fa_model;
        $author_id      = $_POST['author_id'];
        $follower_email = $_POST['follower_email'];
        $follower_id    = is_user_logged_in() ? get_current_user_id() : rand(1,self::$max_id);

        //check if user already following this author
        if( $fa_model->isUserAlreadyFollowing( $author_id, $follower_email ) ){
            echo json_encode(['following' => true], JSON_FORCE_OBJECT);
        }else{
            $fa_model->FollowAuthor($author_id, $follower_email, $follower_id);
            echo json_encode(['following' => true, 'register' => true], JSON_FORCE_OBJECT);
        }

    }
}
add_action('init', function(){new FA_Ajax();});
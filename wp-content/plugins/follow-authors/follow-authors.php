<?php
/*
* Plugin Name:	Follow Authors
* Plugin URI:	https://drive.google.com/open?id=0B3RmFEM0zy_2SzJ4YkxWbXhzbW8
* Description:	This plugin allows users/visitors to follow their favorite author(s) and get notifications whenever new post publish by them
* Version:		1.0
* Author:		Muhammad Waseem
* Author URI:	https://www.facebook.com/engineer.m.waseem
*/

//Do not execute this outside WordPress
if ( ! class_exists( 'WP' ) ) 
{
	die();
}

define('FA_PLUGIN_NAME', plugin_basename(__FILE__));
define('FA_PLUGIN_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('FA_PLUGIN_VERSION', '1.0');

require_once dirname(__FILE__) . '/classes/model/fa_model.php';
require_once dirname(__FILE__) . '/classes/setting.php';
require_once dirname(__FILE__) . '/classes/template.php';
require_once dirname(__FILE__) . '/author_card.php';
require_once dirname(__FILE__) . '/fa_ajax.php';

/*
* @since 1.0
*/
class FA_Follow
{

	/*
	* @var string the name of the table
	*/
	static private $table_name = 'fa_follow_authors';

	/*
	* @var array the options to store
	*/
	static private $options = ['fa_follow_db_version'];

	/*
	* Autoload plugin hooks
	*
	* @since 1.0
	*/
	public function __construct()
	{
		register_activation_hook(FA_PLUGIN_NAME, array(__CLASS__, 'activatePlugin'));
		register_deactivation_hook(FA_PLUGIN_NAME, array(__CLASS__, 'deactivatePlugin'));
		register_uninstall_hook(__FILE__, array(__CLASS__, 'uninstallPlugin'));

        //add_action('new_to_publish', array(__CLASS__, 'notifyAuthorFollowers'), 10, 1);
        //add_action('draft_to_publish', array(__CLASS__, 'notifyAuthorFollowers'), 10, 1);

		add_action('init', array(__CLASS__, 'loadTextDomain'));

        if(is_admin())
        {
            add_action('admin_enqueue_scripts', array(__CLASS__, 'adminEnqueueScripts'));
            add_action('admin_enqueue_scripts', array(__CLASS__, 'adminEnqueueStyles'));
        }
        else
        {
            add_action('wp_enqueue_scripts', array(__CLASS__, 'siteEnqueueScripts'));
            add_action('wp_enqueue_scripts', array(__CLASS__, 'siteEnqueueStyles'));
        }
	}

	/*
	* Activate the plugin
	* 
	* @since 1.0
	*/
	public static function activatePlugin()
	{
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		self::$table_name = $wpdb->prefix . self::$table_name;

		$table = self::$table_name;

		if($wpdb->get_var("SHOW TABLES LIKE '$table' ") != $table)
		{
			$sql = "CREATE TABLE IF NOT EXISTS $table (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`author_id` int(11) NOT NULL,
				`follower_id` int(11) NOT NULL,
				`follower_email` varchar(120) NOT NULL,
				`status` VARCHAR( 10 ) NOT NULL DEFAULT 'active' COMMENT '''active'',''pending''',
				`created_at` datetime NOT NULL,
				`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`),
				KEY `author_id` (`author_id`,`follower_id`)
			) $charset_collate;";

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			
			dbDelta( $sql );
		}
		update_option(self::$options[0], FA_PLUGIN_VERSION);
	}

	/*
	 * Send mails when post status change from new to publish and draft to publish
	 * @since 1.0
	 */
    public static function notifyAuthorFollowers($post)
    {
        global $fa_model;
        global $fa_template;

        $author_id      =   $post->post_author;
        $author_name    =   get_the_author_meta('display_name',$author_id);

        if($fa_model->getTotalFollowers($author_id) != 0){

            $followers = $fa_model->getFollowers($author_id);

            foreach ($followers as $follower){
                $follower_id    =   $follower->follower_id;
                if($follower_id != 0)
                    $follower_email =  $follower->follower_email;

                $post_message = '';

                $params = array('post_id' => $post->ID, 'author_id' => $author_id, 'follower_email' => $follower_email, );

                $post_message .= $fa_template->renderTemplate('mail', $params);

                //@line 135 this need more configuration
                $mail_settings = array('sender_name' => 'Admin', 'sender_email' => 'admin@site.com');

                $parse = parse_url(get_option('siteurl'));
                $blog_host = $parse['host'];

                $sender_name    =   $mail_settings['sender_name'];
                $sender_email   =   'no-reply@' . $blog_host;

                $headers  =   '';

                $headers .= "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type: text/html; charset=".get_bloginfo('charset')."" . "\r\n";
                $headers .= "From: ".$sender_name." <".$sender_email.">" . "\r\n";

                $subject  = 'New Post from ' . $author_name;

                if( wp_mail( $follower_email , $subject, $post_message, $headers) ){
                    //email sent
                }else{
                    //email sending error
                    error_log('Email sending failed at line '. __LINE__);
                }
            }
        }

	}
	/*
	* Load the translation text for the plugin
	* 
	* @since 1.0
	*/
	public static function loadTextDomain()
	{
		load_plugin_textdomain('fa-follow-authors', FALSE, dirname(plugin_basename(__FILE__)).'/languages/');
	}

	/*
	 * Load scripts in WordPress admin dashboard
	 * @since 1.0
	 */
    public static function adminEnqueueScripts()
    {

	}
	
	/*
	 * Load styles in WordPress admin dashboard
	 * @since 1.0
	 */
    public static function adminEnqueueStyles()
    {
        
	}
	
	/*
	 * Load scripts in site
	 * @since 1.0
	 */
    public static function siteEnqueueScripts()
    {
        wp_enqueue_script('jquery');
        wp_enqueue_script('author-card', plugins_url('js/follow-card.js', __FILE__), array('jquery'), '1.0', true);
        wp_enqueue_script('follow-author', plugins_url('js/follow-author.js', __FILE__), array('jquery', 'author-card'), '1.0', true);
        wp_localize_script('follow-author', 'fa_ajax_support', array('ajax_url' => admin_url('admin-ajax.php')));
	}
	
	/*
	 * Load styles in site
	 * @since 1.0
	 */
    public static function siteEnqueueStyles()
    {
        wp_enqueue_style('author-card', untrailingslashit(plugin_dir_url(__FILE__)).'/css/author-card.css', false, '1.0');
	}
	/*
	* Deactivate the plugin
	* 
	* @since 1.0
	*/
	public static function deactivatePlugin()
	{
		//delete option value
		delete_option(self::$options[0]);
	}

	/*
	* uninstall the plugin
	* 
	* @since 1.0
	*/
	public static function uninstallPlugin()
	{
		
	}
}
new FA_Follow;
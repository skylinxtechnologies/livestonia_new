<?php
/*
 * Template class renders the template for sending in email
 * @since 1.0
 */
class FA_Template
{
    /*
     * @var string $template_dir the template directory
     */
    static private $template_dir;

    /*
     * Autoload the template path
     * @since 1.0
     */
    public function __construct()
    {
        self::$template_dir = FA_PLUGIN_DIR . 'templates' . DIRECTORY_SEPARATOR;
    }

    /*
     * Render the template
     *
     * @param string $default the default name for template
     * @param array $params the parameters
     * @since 1.0
     *
     * @return mix
     */
    public function renderTemplate($default = 'mail', $params)
    {
        $file = self::$template_dir . $default . '.php';
        $data = array('data' => $params);

        ob_start();
        include_once($file);
        $output = ob_get_contents();
        ob_end_clean();

        return $output;

    }
}
$fa_template = new FA_Template;
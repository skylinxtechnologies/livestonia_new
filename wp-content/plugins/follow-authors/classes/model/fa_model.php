<?php

/*
* The model class that will process database operations
*
* @since 1.0
*/

class FA_Model
{
	/*
	* @var string the name of the table
	*/
	static private $table_name = 'fa_follow_authors';

	/*
	* @var string users table
	*/
	static private $users_table = 'users';

	/*
	* @var string wordpress database prefix
	*/
	static private $prefix;

    /*
     * Autoload the table value
     * @since 1.0
     */
	public function __construct()
	{
		global $wpdb;

		self::$prefix = $wpdb->prefix;
		self::$table_name = self::$prefix . self::$table_name;
		self::$users_table = self::$prefix . self::$users_table;
	}

    /*
     * Check if follower is already following author with this id
     *
     * @since 1.0
     * @param integer $author_id the author id
     * @param string $follower_email the follower email address
     *
     * @return boolean
     */
    public function isUserAlreadyFollowing($author_id, $follower_email)
    {
        global $wpdb;

        $is_following = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM " . self::$table_name . " WHERE author_id = %d AND follower_email = %s",$author_id, $follower_email ) );
        return ($is_following == 0) ? false : true;
    }

    /*
     * Insert follower and author record
     *
     * @since 1.0
     * @param integer $author_id the author id
     * @param string $follower_email the follower email address
     * @param integer $follower_id the follower id
     */
    public function FollowAuthor($author_id, $follower_email, $follower_id)
    {
        global $wpdb;

        $wpdb->insert(self::$table_name, array('author_id' => $author_id, 'follower_id' => $follower_id, 'follower_email' => $follower_email, 'created_at' => current_time('mysql')), array('%d', '%d', '%s'));
    }

    /*
     * Get total number of followers by author id
     *
     * @since 1.0
     * @param integer $author_id the author id
     *
     * @return integer
     */
    public function getTotalFollowers($author_id)
    {
        global $wpdb;

        return $total = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(follower_id) FROM " . self::$table_name . " WHERE author_id = %d AND status = %s", $author_id, 'active') );
    }

    /*
     * Get all followers of the author
     *
     * @since 1.0
     * @param integer $author_id the author id
     *
     * @return mix
     */
    public function getFollowers($author_id)
    {
        global $wpdb;

        return $followers = $wpdb->get_results( $wpdb->prepare("SELECT follower_id, follower_email FROM " . self::$table_name . " WHERE author_id = %d AND status = %s", $author_id, 'active') );
    }
}
$fa_model = new FA_Model;
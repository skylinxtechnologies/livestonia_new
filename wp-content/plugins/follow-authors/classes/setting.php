<?php

/*
* The model class that will process database operations
*
* @since 1.0
*/

class FA_Setting
{
    /*
    * @var string store general settings key
    */
    static private $general_settings_key = 'fa_general_settings';

    /*
    * @var string store mail settings key
    */
    static private $mail_settings_key = 'fa_mail_settings';

    /*
    * @var string store options key
    */
    static private $plugin_options_key = 'fa_plugin_options';

    /*
     * @var array store general settings key value
     */
    static private $general_settings;

    /*
     * @var array store mail settings key value
     */
    static private $mail_settings;

    /*
    * @var array store plugin settings tabs' names
    */
    static private $plugin_settings_tabs = array();

    /*
    * Autoloads the hooks
    * @since 1.0
    */
    public function __construct()
    {
        add_action('init', array(__CLASS__, 'loadSettings'));
        add_action('admin_init', array(__CLASS__, 'registerGeneralSettings'));
        add_action('admin_menu', array(__CLASS__, 'addAdminMenus'));
        add_filter('plugin_action_links_' . FA_PLUGIN_NAME, array(__CLASS__, 'pluginSettingsLink'));
    }

    /*
    * Fetch options value from database
    * @since 1.0
    */
    public static function loadSettings()
    {
        self::$general_settings = (array)get_option(self::$general_settings_key);
        self::$mail_settings = (array)get_option(self::$mail_settings_key);
    }

    /*
    * Load General tab settings page in settings menu
    * @since 1.0
    */
    public static function registerGeneralSettings()
    {
        self::$plugin_settings_tabs[self::$general_settings_key] = __('General', 'fa-follow-authors');
        register_setting(self::$general_settings_key, self::$general_settings_key);
        add_settings_section('fa_section_general', __('General Settings', 'fa-follow-authors'), array(__CLASS__, 'sectionGeneralDescription'), self::$general_settings_key);
        add_settings_field('show_on_card', __('Show on card', 'fa-follow-authors'), array(__CLASS__, 'fieldShowOnCard'), self::$general_settings_key, 'fa_section_general');
    }

    /*
     * Display description for general section in settings page
     * @since 1.0
     */
    public static function sectionGeneralDescription()
    {
        echo '<p><em>Following information can be shown on author card.</em></p>';
    }

    /*
     * Show fields for section
     * @since 1.0
     */
    public static function fieldShowOnCard()
    {
        $show_on_card = (isset(self::$general_settings['show_on_card']) ? (self::$general_settings['show_on_card']) : '');
        $fields = array('first_name' => 'First Name', 'last_name' => 'Last Name', 'nickname' => 'Nick Name', 'description' => 'About');
        ?>
        <ul>
            <?php foreach ($fields as $key => $row): ?>
                <li>
                    <input type="checkbox"
                           name="<?php echo self::$general_settings_key; ?>[show_on_card][<?php echo $key; ?>]" <?php echo(!empty($show_on_card[$key]) ? 'checked="checked"' : ''); ?>>
                    <label><?php echo $row; ?></label>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php
    }

    /*
     * Add menu to admin settings page
     * @since 1.0
     */
    public static function addAdminMenus()
    {
        add_options_page(__('Follow Author', 'fa-follow-authors'), __('Follow Author', 'fa-follow-authors'), 'manage_options', self::$plugin_options_key, array(__CLASS__, 'pluginOptionsPage'));
    }

    /*
     * Show plugin setting page contents
     * @since 1.0
     */
    public static function pluginOptionsPage()
    {
        $tab = isset($_GET['tab']) ? $_GET['tab'] : self::$general_settings_key;
        ?>
        <div class="wrap">
            <?php self::pluginOptionsTab(); ?>
            <form action="options.php" method="post">
                <?php wp_nonce_field('update-options'); ?>
                <?php settings_fields($tab); ?>
                <?php do_settings_sections($tab); ?>
                <?php submit_button(); ?>
            </form>
        </div>
        <?php
    }

    /*
     * Show tabs for plugin page
     * @since 1.0
     */
    public static function pluginOptionsTab()
    {
        $current_tab = isset($_GET['tab']) ? $_GET['tab'] : self::$general_settings_key;
        get_screen_icon();
        echo '<h2 class="nav-tab-wrapper">';
        foreach (self::$plugin_settings_tabs as $key => $value) {
            $active = $current_tab == $key ? 'nav-tab-active' : '';
            echo '<a class="nav-tab ' . $active . '" href="?page=' . self::$plugin_options_key . '&tab=' . $key . '">' . $value . '</a>';
        }
        echo '</h2>';
    }

    /*
     * Show settings page on plugins directory
     * @since 1.0
     */
    public static function pluginSettingsLink($links)
    {
        $settings_link = '<a href="options-general.php?page=' . self::$plugin_options_key . '">' . __('Settings', 'fa-follow-authors') . '</a>';
        array_unshift($links, $settings_link);
        return $links;
    }

    /*
     * @since 1.0
     */
    public static function getGeneralSettingsKey()
    {
        return self::$general_settings_key;
    }

    /*
     * @since 1.0
     */
    public static function getMailSettingsKey()
    {
        return self::$mail_settings_key;
    }

    /*
     * @since 1.0
     */
    public static function getPluginOptionsKey()
    {
        return self::$plugin_options_key;
    }

    /*
     * @since 1.0
     */
    public static function getGeneralSettings()
    {
        return self::$general_settings;
    }

    /*
     * @since 1.0
     */
    public static function getMailSettings()
    {
        return self::$mail_settings;
    }

    /*
     * @since 1.0
     */
    public static function getPluginSettingsTabs()
    {
        return self::$plugin_settings_tabs;
    }
}
add_action('plugins_loaded', function(){new FA_Setting;});
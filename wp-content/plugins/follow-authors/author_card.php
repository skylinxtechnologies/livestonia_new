<?php
/*
 * Author_Card class for managing author card and its operations
 * @since 1.0
 */
class Author_Card
{
    /*
     * @since 1.0
     */
    public function __construct()
    {
        add_filter('the_author', array(__CLASS__, 'modifyPostAuthor'), 10, 1);
    }

    /*
     * The hook to apply author card features
     * @since 1.0
     * @param string $author_name the post author name
     * @return string
     */
    public static function modifyPostAuthor($author_name)
    {
        if(! is_singular())
            return $author_name;
        return self::getAuthorCardTemplate($author_name);
    }

    /**
     * The author card template
     * @since 1.0
     * @param string $author_name the author name
     * @return string
     */
    public static function getAuthorCardTemplate($author_name)
    {
        global $fa_model;
        $general_settings = FA_Setting::getGeneralSettings();
        if( !isset($general_settings['show_on_card']) ){
            return $author_name;
        }
        $post_id = get_the_id();
        $post_author_id = get_the_author_meta('ID');
        $total_followers = $fa_model->getTotalFollowers($post_author_id);
        $post_author_url = get_author_posts_url($post_author_id);
        
        foreach($general_settings['show_on_card'] as $key=>$value)
        {
            if($value == 'on')
                $author_meta[$key] = get_the_author_meta($key, $post_author_id);
        }
        $post_author_avatar = get_avatar($post_author_id);
        $table_info = '<table class="table-info">';
        foreach ($author_meta as $key => $value)
        {
            $table_info .= '<tr>';
            $table_info .= "<th>".ucfirst(str_replace('_', ' ', $key))."</th><td>".substr($value, 0, 22)."</td>";
            $table_info .= '</tr>';
        }
        $table_info .= '</table>';
        $author_url = get_author_posts_url($post_author_id, get_the_author_meta('user_nicename'));
        $input_placeholder = __('Enter your email', 'fa-follow-authors');
        $follow_btn = __('Follow me', 'fa-follow-authors');
        $html = <<<EOD
        <a href="{$post_author_url}" class="author-card-link">{$author_name}</a>
        <div class="author-card">
            <div class="card-avatar">
                {$post_author_avatar}
            </div>
            <div class="card-info">
                {$table_info}
            </div>
            <div class="card-status">
                Followers <span>{$total_followers}</span>
            </div>
            <div class="card-follow">
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#followModal">Follow</button>
            </div>
        </div>
        <div id="followModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Follow <strong>{$author_meta['first_name']}</strong></h4>
                    </div>
                    
                    <div class="modal-body">
                        <div class="modal-messages">
                        </div>
                        <div class="form-group">
                            <input type="text" name="follower_email" placeholder="{$input_placeholder}" class="form-control">
                            <input type="hidden" name="author_id" value="{$post_author_id}">
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="submitFollowBtn">{$follow_btn}</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
EOD;
        return $html;
    }
}
new Author_Card;

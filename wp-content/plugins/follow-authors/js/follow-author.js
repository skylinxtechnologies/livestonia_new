//process author ajax operations
function validateEmail(field)
{
    var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( !filter.test(field.val()) ){
        field.focus();
        jQuery('.modal-messages').show().html('Please enter valid email address').delay(2000).fadeOut('slow');
        return false;
    }
    else{
        return true;
    }
}

//show custom message popups
function showMessage($message, $status) {
    var old_html = jQuery('.modal-body').html();
    if($status == true)
        jQuery('.modal-body').html($message).css('color', '#5FBA7D');
    else
        jQuery('.modal-body').html($message);
    setTimeout(function(){
        jQuery('#followModal').modal('hide');
        jQuery('.modal-body').html(old_html);
    }, 2000);

}
jQuery(document).ready(function($){
    //send data through ajax
    $("#submitFollowBtn").click(function(){
        var author_id = $('input[name="author_id"]').val();
        var follower_email = $('input[name="follower_email"]');

        //validate email
        if( validateEmail(follower_email) !== false){
            var follower_email_value = follower_email.val();
        }else{
            return false;
        }
        $.ajax({
            url:        fa_ajax_support.ajax_url,
            data:       {action:'fa_follow_author', author_id:author_id, follower_email: follower_email_value},
            type:       'post',
            beforeSend: function(){
                console.log('Beginning sending data..');
            },
            success:    function (data, textStatus, jqXHR) {
                var obj = data.slice(0, -1);
                obj = JSON.parse(obj);
                if(obj.following == true && obj.register == true){
                    showMessage('You have successfully followed the author.', true);
                }else if( obj.following == true){
                    showMessage('You are already following the author.', false);
                }
            },
            error:      function (jqXHR, textStatus, errorThrown) {
                
            }
        });
    });
});
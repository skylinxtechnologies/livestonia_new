//author card scripting
jQuery(document).ready(function($){
    var author_card_link = $('.author-card-link');
    var author_card = $('.author-card');
    author_card_link.closest('div').addClass('pos-relative');
    author_card_link.hover(function(event){
        //on mouse over
        if(event.type == 'mouseenter') {
            author_card.addClass('active');
        }
    });
    author_card.on('mouseleave', function(){
        author_card.removeClass('active');
    });
    $(this).mouseup(function(e){
        if( !author_card.is(e.target) && author_card.has(e.target).length === 0){
            author_card.removeClass('active');
        }
    });
});
<?php
/*
 * PLugin Name: Newsletter Compilation
 * Plugin URI:  https://drive.google.com/open?id=0B3RmFEM0zy_2MklpWGU0Z1dUZFE
 * Description: Compile newsletter based upon events starting/ending date intervals
 * Version:     1.0
 * Author:      Muhammad Waseem
 * Author URI:  https://www.facebook.com/engineer.m.waseem
 */

/*
 * Do not execute this plugin outside WordPress
 */
if( ! class_exists('WP') ){
    die();
}

/*
 * News_Compile
 * It process newsletters based upon events
 */
abstract class News_Compile
{
    private static $post = null;

    public static function init()
    {
        add_action('init', array(self::class, 'loadDomainText'));
        //add_action('admin_init', array(self::class, 'createPluginPage'));
        add_action('admin_menu', array(self::class, 'myPluginMenu'));
        if(is_admin()){
            add_action('admin_enqueue_scripts', array(self::class, 'adminEnqueueScripts'));
            add_action('admin_enqueue_scripts', array(self::class, 'adminEnqueueStyles'));
        }
        add_action('admin_post_compile_newsletter', array(self::class, 'handleCompileNewsletter'));

        //ajax for compiling newsletters
        add_action('wp_ajax_nopriv_compile_newsletter', array(self::class, 'handleCompileNewsletter'));
        add_action('wp_ajax_compile_newsletter', array(self::class, 'handleCompileNewsletter'));

        //ajax for showing newsletter previews
        add_action('wp_ajax_nopriv_newsletter_previews', array(self::class, 'handleNewsletterPreviews'));
        add_action('wp_ajax_newsletter_previews', array(self::class, 'handleNewsletterPreviews'));

        //ajax for sending newsletter to emails
        add_action('wp_ajax_nopriv_newsletter_send_email', array(self::class, 'handleEmailSending'));
        add_action('wp_ajax_newsletter_send_email', array(self::class, 'handleEmailSending'));
    
        //adding phpmailer smtp settings
        //add_action('phpmailer_init', array(self::class, 'initializePHPMailer'));
    }

    /*public static function initializePHPMailer($phpmailer)
    {
        $phpmailer->IsSMTP();
        $phpmailer->SMTPAuth = true;
        $phpmailer->Host = 'aspmx.l.google.com';
        $phpmailer->Port = 25;
        $phpmailer->Username = 'waseem.skylinx@gmail.com';
        $phpmailer->Password = 'skylinx786+';
    }*/

    public static function loadDomainText()
    {
        load_plugin_textdomain( 'news-compile', false, dirname(plugin_basename(__FILE__)).'/languages/' );
    }

    public static function createPluginPage()
    {
        $post = new WP_Query(['pagename'=>'newsletter-preview']);
        if($post->found_posts == 0){
            $plugin_page_attr = array(
                'post_title'    =>  'Newsletter Preview',
                'post_content'  =>  '',
                'post_status'   =>  'publish',
                'post_author'   =>  get_current_user_id(),
                'post_type'     =>  'page',
                'comment_status'=>  'closed'
            );
            $post = wp_insert_post($plugin_page_attr, true);
        }else{
            self::$post = $post->posts[0];
        }
    }

    public static function adminEnqueueScripts()
    {
        wp_enqueue_script('jquery');
        wp_enqueue_script('semantic-ui', 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.min.js', array('jquery'), '1.0', true);
        wp_enqueue_script('compile-news', plugins_url('js/compile-news.js', __FILE__), array('jquery', 'semantic-ui'), '1.0', true);
        wp_localize_script('compile-news', 'press_logo_path', get_stylesheet_directory_uri() . '/uploads/logo.png');
    }

    public static function adminEnqueueStyles()
    {
        wp_enqueue_style('semantic-ui', 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.min.css', false, '1.0');
        wp_enqueue_style('compile-news', untrailingslashit(plugin_dir_url(__FILE__)).'/css/compile-news.css', 'semantic-ui', '1.0');
    }

    public static function myPluginMenu()
    {
        add_submenu_page('edit.php', __('Compile Newsletters', 'news-compile'), __('Compile Newsletters', 'news-compile'), 'edit_posts', 'compile_newsletter', array(self::class, 'showCompileNewslettersPage'));
        //add_submenu_page('options-general.php', __('Newsletter Previews', 'news-compile'), __('Newsletter Previews', 'news-compile'), 'manage_options', 'newsletter_previews', array(self::class, 'showNewsletterPreviewsPage'));
    }

    public static function showNewsletterPreviewsPage()
    {
        print_r($_REQUEST);
    }
    public static function showCompileNewslettersPage()
    {
        global $wpdb;

        $events_table = $wpdb->prefix . 'em_events';

        $events = $wpdb->get_results("SELECT *
            FROM $events_table
            ORDER BY event_start_time
        ");

        $new_events = $events;
        $first_event = array_shift($new_events);
        $last_event  = end($new_events);

       ?>
        <div class="wrap">
            <div class="ui container">
                <h1 class="ui header">
                    <i class="newspaper icon"></i>
                    <div class="content">Compile Newsletters</div>
                </h1>
                <p>Please select <strong>start</strong> and <strong>end</strong> dates for events and click <strong>compile</strong>.</p>
                <div class="ui stackable grid">
                    <div class="sixteen wide column">
                        <form action="" method="post" id="compile_newsletter_form">
                            <input type="hidden" name="action" value="compile_newsletter">
                            <?php wp_nonce_field(); ?>
                            <div class="ui form">
                                <div class="inline fields">
                                    <div class="field">
                                        <label for="start_date">Start Date</label>
                                        <input type="date" id="start_date" min="<?php echo date('Y-m-d', strtotime($first_event->event_start_date)); ?>" max="<?php echo date('Y-m-d', strtotime($last_event->event_end_date)); ?>" value="<?php echo date('Y-m-d', strtotime($first_event->event_start_date)); ?>">
                                    </div>
                                    <div class="field">
                                        <label for="end_date">End Date</label>
                                        <input type="date" id="end_date" min="<?php echo date('Y-m-d', strtotime($first_event->event_start_date)); ?>" max="<?php echo date('Y-m-d', strtotime($last_event->event_end_date)); ?>" value="<?php echo date('Y-m-d', strtotime($last_event->event_end_date)); ?>">
                                    </div>
                                    <div class="field">
                                        <input type="submit" class="ui primary button" value="Filter Events">
                                    </div>
                                </div>
                            </div><!-- /.form -->
                        </form>
                    </div><!-- /.column -->
                </div><!-- /.grid -->

                <div class="ui stackable grid">
                    <div class="sixteen wide column">
                        <table class="ui compact celled table" id="compile_newsletter_table">
                            <thead class="full-width">
                                <tr>
                                    <th style="width: 15px;">
                                        <input type="checkbox" name="selection[]" value="-1">
                                    </th>
                                    <th>Title</th>
                                    <th>Languages</th>
                                    <th>Location</th>
                                    <th>Date and Time</th>
                                    <th>Owner</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($events as $event):?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="selection[]" value="<?php echo $event->event_id; ?>">
                                        </td>
                                        <td><?php echo self::getEventTitleWithURI($event); ?></td>
                                        <td><?php echo self::getEventLanguage($event);?></td>
                                        <td><?php echo self::getEventLocation($event);?></td>
                                        <td><?php echo self::getEventTiming($event); ?></td>
                                        <td><?php echo self::getEventAuthorWithURI($event); ?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="6">
                                        <div class="ui right floated small primary labeled icon button" id="check-newsletter-preview">
                                            <i class="send icon"></i> Check Newsletter Preview
                                        </div>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.column -->
                </div><!-- /.grid -->

                <div class="ui stackable grid" id="newsletter-previews">
                    <div class="eight wide column">
                        <div class="ui two cards"></div>
                    </div><!-- /.column -->
                </div><!-- /.grid -->
            </div>
        </div>
        <?php
    }

    public static function handleCompileNewsletter()
    {
        global $wpdb;

        $start_date = $_POST['start_date'];
        $end_date   = $_POST['end_date'];

        $events_table = $wpdb->prefix.'em_events';

        $events = $wpdb->get_results( $wpdb->prepare("
        SELECT `event_id`, `event_name`, `post_id`, `event_start_time`, `event_end_time`, `event_start_date`, `event_end_date`
        FROM $events_table
        WHERE 
          event_start_date >= '%s' AND event_end_date <= '%s'
        ", $start_date, $end_date) );

        foreach($events as &$event){

            $event->language = self::getEventLanguage($event);
            $event->title = self::getEventTitleWithURI($event);
            $event->date_and_time = self::getEventTiming($event);
            $event->location = self::getEventLocation($event);
            $event->owner = self::getEventAuthorWithURI($event);
        }
        if(count($events) > 0)
            echo json_encode($events, JSON_FORCE_OBJECT);
        else
            echo json_encode(['status'=>false], JSON_FORCE_OBJECT);
    }

    public static function handleNewsletterPreviews()
    {
        global $wpdb;

        $events = $_POST['events'];

        $events_table = $wpdb->prefix.'em_events';
        $posts_table = $wpdb->prefix.'posts';

        $events = str_replace(' ', '', implode(",",$events));

        $events = $wpdb->get_results("SELECT ID, $posts_table.post_content FROM $events_table, $posts_table WHERE event_id IN(".$events.") AND $posts_table.ID = $events_table.post_id");

        foreach($events as &$event){
            $post = new WP_Query(['p'=>$event->ID, 'post_type'=>'event']);
            $post->the_post();

            $event->post_title = self::getEventTitle( get_the_title($event->ID) );
            $event->post_content = self::getEventTitle( $event->post_content );
            $event->post_thumbnail =  wp_get_attachment_url( get_post_thumbnail_id() );
            $event->post_url = get_permalink();
        }

        echo json_encode($events, JSON_FORCE_OBJECT);
    }

    public static function addRowsWithData($event1, $event2)
    {
        if(!isset($event2)){
            return '<tr><td colspan="2" style="overflow: hidden;width: 250px;margin:0;padding:4px;"><a href="'.$event1['post_url'].'" style="text-decoration:none;"><div style="background:url(\''.$event1['post_thumbnail'].'\'); height:150px; width:100%; background-size:contain;"><div class="content"><h2 style="text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;">'.$event1['post_title'].'</h2><p style="text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;">'.$event1['post_content'].'</p></div></div></a></td></tr>';
        }
        return '<tr> <td style="overflow: hidden;width: 250px;margin:0;padding:4px;"><a href="'.$event1['post_url'].'" style="text-decoration:none;"><div style="background:url(\''.$event1['post_thumbnail'].'\'); height:150px; width:100%; background-size:contain;"><div class="content"><h2 style="text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;">'.$event1['post_title'].'</h2><p style="text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;">'.$event1['post_content'].'</p></div></div></a></td>      <td style="overflow: hidden;width: 250px;margin:0;padding:4px;"><a href="'.$event2['post_url'].'" style="text-decoration:none;"><div style="background:url(\''.$event2['post_thumbnail'].'\'); height:150px; width:100%; background-size:contain;"><div class="content"><h2 style="text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;">'.$event2['post_title'].'</h2><p style="text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;">'.$event2['post_content'].'</p></div></div></a></td></tr>';
    }
    public static function handleEmailSending()
    {
        $events = $_POST['events'];

        $newsletter_rows = '';
        $limit = count($events);
        
        for($i=0; $i<$limit;){
            $j = $i+1;
            $newsletter_rows .= self::addRowsWithData($events[$i], $events[$j]);
            $i=$j+1;
        }
       
        $logo_path = get_stylesheet_directory_uri() . '/uploads/logo.png';
        $html = <<<EOD
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>This Week</title>
</head>
<body>
    <table style="border-collapse: collapse;border:1px solid #ddd; margin-left:25px;">
        <thead>
        <tr>
            <th style="overflow: hidden;width: 250px;margin:0;padding:4px;"><img src="{$logo_path}" alt="logo" style="width: 100%;"></th>
            <th style="overflow: hidden;width: 250px;margin:0;padding:4px;color:#ffffff;" bgcolor="#CF202C">This Week</th>
        </tr>
        </thead>
        <tbody>
        {$newsletter_rows}
        </tbody>
    </table>
</body>
</html>
EOD;

    echo json_encode([self::sendMailToSubscribers($html)], JSON_FORCE_OBJECT);
    
    }

    public static function sendMailToSubscribers($template)
    {
        global $wpdb;

        $emaillist_table = $wpdb->prefix . 'es_emaillist';

        $emaillist = $wpdb->get_results("
            SELECT $emaillist_table.es_email_mail
            FROM $emaillist_table 
            WHERE 
              es_email_group NOT IN('public', 'Public') AND 
              es_email_status = 'Confirmed'
        ");

        $new_list = array();
        foreach($emaillist as $email){$new_list[] = $email->es_email_mail;}
       


        $to = $new_list;
        $subject = 'Livestonia - This Week';
        $body = $template;
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $output = wp_mail($to, $subject, $body, $headers);
        return count($new_list);
    }

    /*
     * Event filter helper functions
     */
    public static function getEventTitleWithURI($event = '')
    {
        if(! is_object($event) ){
            return false;
        }
        return '<a href="'.get_post_permalink($event->post_id).'" target="_blank">'.self::getEventTitle($event->event_name).'</a>';
    }

    public static function getEventAuthorWithURI($event = '')
    {
        if(! is_object($event) ){
            return false;
        }
        return '<a href="'.get_author_posts_url(get_post_field('post_author', $event->post_id)).'" target="_blank">'.get_the_author_meta('first_name', get_post_field('post_author', $event->post_id)) . ' ' . get_the_author_meta('last_name', get_post_field('post_author', $event->post_id)).'</a>';
    }
    public static function getEventLocation($event = '')
    {
        if(! is_object($event) ){
            return false;
        }

        global $wpdb;
        $locations_table = $wpdb->prefix . 'em_locations';

        return $wpdb->get_var( $wpdb->prepare("SELECT `location_name` AS location FROM $locations_table WHERE  location_id = '%d'", get_post_meta($event->post_id, '_location_id')[0]) );
    }

    public static function getEventDate($date=array())
    {
        if( !is_array($date) )
            return false;
        return date('j.M Y', strtotime($date[0]));
    }

    public static function getEventTiming($event = '')
    {
        if(! is_object($event) ){
            return false;
        }

        if(get_post_meta($event->post_id, '_event_all_day')[0] == 1)
            return date('j.M Y', strtotime(get_post_meta($event->post_id, '_event_start_date')[0])) . ' - ' . date('j.M Y', strtotime(get_post_meta($event->post_id, '_event_end_date')[0])) . '<br>' . 'All day';
        else
            return date('j.M Y', strtotime(get_post_meta($event->post_id, '_event_start_date')[0])) . ' - ' . date('j.M Y', strtotime(get_post_meta($event->post_id, '_event_end_date')[0])) . '<br>' . date('h:i A', strtotime(get_post_meta($event->post_id, '_event_start_time')[0])) . ' - '. date('h:i A', strtotime(get_post_meta($event->post_id, '_event_end_time')[0]));
    }

    public static function getEventTitle($title)
    {
        if(!is_string($title))
            return false;
        return preg_replace('/(\[.)+[\w]*[\W]+(\])?/i', '', $title);
    }

    public static function getEventLanguage($event = '')
    {
        if(! is_object($event) ){
            return false;
        }

        if($event->event_name != ''){
            preg_match('/\[(\:[^\]]+)\]/i', $event->event_name, $matches);
            $matches = preg_replace('/[\:]+/i', '', $matches[1]);
            switch ($matches){
                case 'en':
                    return 'English';
                    break;
                case 'et':
                    return 'Estonian';
                    break;
                case 'ru':
                    return 'Russian';
                    break;
            }
        }else{
            return 'No language set';
        }
        
    }

}
add_action('plugins_loaded', array('News_Compile', 'init'));
//process ajax request/response
jQuery(document).ready(function($){

    window.event_global_object = new Object();
    function addPreviewCards(obj1, obj2) {
        obj1.post_thumbnail = (obj1.post_thumbnail != false) ? obj1.post_thumbnail : 'https://dummyimage.com/250x150/F1F1F1/000000.png&text=placeholder';

       if(typeof obj2 == 'undefined'){
            
            $("#newsletter-previews .eight.wide.column table#preview").append("<tr><td colspan='2' style='overflow:hidden;width:250px;margin:0;padding:4px;'><a href='"+obj1.post_url+"' style='text-decoration:none;'><div style='background:url(\""+obj1.post_thumbnail+"\");height:150px; width:100%; background-size:cover;'><div id='content'><h2 style='text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;'>"+obj1.post_title+"</h2><p style='text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;'>"+obj1.post_content+"</p></div></div></a></td></tr>");
            
           return;
       }
        obj2.post_thumbnail = (obj2.post_thumbnail != false) ? obj2.post_thumbnail : 'https://dummyimage.com/250x150/F1F1F1/000000.png&text=placeholder';
        
        $("#newsletter-previews .eight.wide.column table#preview").append("<tr> <td style='overflow:hidden;width:250px;margin:0;padding:4px;'><a href='"+obj1.post_url+"' style='text-decoration:none;'><div style='background:url(\""+obj1.post_thumbnail+"\");height:150px; width:100%; background-size:cover;'><div id='content'><h2 style='text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;'>"+obj1.post_title+"</h2><p style='text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;'>"+obj1.post_content+"</p></div></div></a></td>   <td style='overflow:hidden;width:250px;margin:0;padding:4px;'><a href='"+obj2.post_url+"' style='text-decoration:none;'><div style='background:url(\""+obj2.post_thumbnail+"\");height:150px; width:100%; background-size:cover;'><div id='content'><h2 style='text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;'>"+obj2.post_title+"</h2><p style='text-decoration:none; width: 100%;margin: 0;padding-left: 5px;overflow: hidden;color: #000000;'>"+obj2.post_content+"</p></div></div></a></td></tr>");
        
        
    }
    //send newsletter preview
    $("#check-newsletter-preview").on('click', function (elm) {
        var values = $("input[type='checkbox'][name='selection\[\]']:checked:not(input\[type='checkbox'\]\[value='-1'\])").map(function () {
            return this.value;
        }).get().join(',');
        values = JSON.parse('['+values+']');
        if(values.length != 0){
            $.ajax({
                url:        ajaxurl,
                data:       {action:'newsletter_previews', events:values},
                type:       'post',
                beforeSend: function(){
                    $("#newsletter-previews .eight.wide.column").empty();
                    console.log('Launching newsletter previews...');
                },
                success:function (data, textStatus, jqXHR) {
                    var obj = data.slice(0, -1);
                    obj = JSON.parse(obj);

                    window.event_global_object = obj;

                    var limit = Object.keys(obj).length;
                    
                    $("#newsletter-previews .eight.wide.column").append('<table id="preview"></table>');
                    $("#newsletter-previews .eight.wide.column table#preview").append('<tr><th style="overflow:hidden;width:250px;margin:0;padding:4px;"><img src=\"'+press_logo_path+'\" alt="logo" style="width:100%;"></th><th style="overflow:hidden;width:250px;margin:0;padding:4px;text-transform:uppercase;background-color:#CF202C;color:#FFFFFF">This Week</th></tr>');
                    
                    for(var i=0; i<limit;){
                        var j=i+1;
                        addPreviewCards(obj[i], obj[j]);
                        i=j+1;
                    }
                    

                    $("#newsletter-previews .eight.wide.column").append("<br><div class=\"ui right floated small positive labeled icon button fluid\" id=\"send-newsletter-btn\"> <i class=\"send icon\"></i> Send Newsletters </div>");

                    $("#send-newsletter-btn").on('click', function(elm){
                        //send email sending request
                        $.ajax({
                            url:        ajaxurl,
                            data:       {action:'newsletter_send_email', events:window.event_global_object},
                            type:       'post',
                            beforeSend: function () {
                                console.log('Email sending request...');
                            },
                            success: function (data, textStatus, jqXHR) {
                                var obj = data.slice(0, -1);
                                obj = JSON.parse(obj);
                                
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(jqXHR);
                            }
                        });
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
                
            });
        }
    });

    //checkbox data table logic
    $("#compile_newsletter_table tbody input[type='checkbox']").on('change', function(){
        var all_checkboxes = $("#compile_newsletter_table tbody input[type='checkbox']");
        if(all_checkboxes.length == all_checkboxes.filter(":checked").length){
            $("#compile_newsletter_table input[type='checkbox'][name='selection\[\]'][value='-1']").prop('checked', true);
        }else if(all_checkboxes.length != all_checkboxes.filter(":checked").length || all_checkboxes.filter(":checked").length == 0){
            $("#compile_newsletter_table input[type='checkbox'][name='selection\[\]'][value='-1']").prop('checked', false);
        }
    });

    $("#compile_newsletter_table input[type='checkbox'][name='selection\[\]'][value='-1']").on('click', function(elm){
        if(elm.target.checked){
            $("#compile_newsletter_table tbody input[type='checkbox']").prop('checked', true);
        }else{
            $("#compile_newsletter_table tbody input[type='checkbox']").prop('checked', false);
        }
    });

    //add rows in data table filled with ajax data
    function addTableRows(obj) {

        $("#compile_newsletter_table").append("" +
            "<tr>" +
            "<td><input type=\"checkbox\" name=\"selection\[\]\" value="+obj.event_id+"></td>" +
            "<td>"+obj.title+"</td>" +
            "<td>"+obj.language+"</td>" +
            "<td>"+obj.location+"</td>" +
            "<td>"+obj.date_and_time+"</td>" +
            "<td>"+obj.owner+"</td>" +
            "</tr>");
    }
    function addEmptyRow(msg) {
        $("#compile_newsletter_table").append("" +
            "<tr>" +
            "<td colspan='5'>"+msg+"</td>" +
            "</tr>");
    }
    $("#compile_newsletter_form").submit(function(e){
        e.preventDefault();
        var start_date = $(this).find('#start_date').val();
        var end_date = $(this).find('#end_date').val();
        var action = $(this).find('input[name="action"]').val();

        $.ajax({
            url:        ajaxurl,
            data:       {action:action, start_date:start_date, end_date: end_date},
            type:       'post',
            beforeSend: function(){
                console.log('Fetching events From ' + start_date + ' to ' + end_date);
            },
            success:function(data, textStatus, jqXHR){
                var obj = data.slice(0, -1);
                obj = JSON.parse(obj);

                $("table#compile_newsletter_table tbody").empty();

                if(obj.status == false){
                    addEmptyRow('No events found.');
                    return;
                }

                $.each(obj, function(i,e){
                    addTableRows(e);
                });
            },
            error:  function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
            }
        });
    });
});
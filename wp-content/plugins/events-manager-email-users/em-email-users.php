<?php

/*

Plugin Name: Events Manager Email Users

Version: 1.2.3

Plugin URI: http://www.grolabo.net

Description: Add email facilities to events manager plugin

Author: Philippe GILLES

Author URI: http://www.petilabo.net

Text Domain: em-email-users

License: GPL2


    Copyright 2014  Philippe GILLES  (email: petilabo@gmail.com)


    This program is free software; you can redistribute it and/or modify

    it under the terms of the GNU General Public License, version 2, as

    published by the Free Software Foundation.


    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

    GNU General Public License for more details.


    You should have received a copy of the GNU General Public License

    along with this program; if not, write to the Free Software

    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

// No direct links allowed

defined('ABSPATH') or die("No script kiddies please !");

// Don't load if is_admin undefined

if ( ! function_exists( 'is_admin' ) ) {

    header( 'Status: 403 Forbidden' );

    header( 'HTTP/1.1 403 Forbidden' );

    exit();

}

// General constants

define( 'EM_EMAIL_USERS_FULL_NAME', "Events Manager Email Users" );

define( 'EM_EMAIL_USERS_SHORT_NAME', "EM Email Users" );

define( 'EM_EMAIL_USERS_SLUG', "em-email-users");

define( 'EM_EMAIL_USERS_OPTIONS', "em-email-users-options");

define( 'EM_EMAIL_USERS_VERSION', '1.0' ); 

// Option names constants

define( 'EM_EMAIL_USERS_OPTION_PENDING_NAME', 'pending-booking');

define( 'EM_EMAIL_USERS_OPTION_CONFIRMED_NAME', 'confirmed-booking');

define( 'EM_EMAIL_USERS_OPTION_REJECTED_NAME', 'rejected-booking');

define( 'EM_EMAIL_USERS_OPTION_CANCELLED_NAME', 'cancelled-booking');

define( 'EM_EMAIL_USERS_OPTION_WAIT_ON_NAME', 'waiting-online');

define( 'EM_EMAIL_USERS_OPTION_WAIT_OFF_NAME', 'waiting-offline');

// Option constants

define( 'EM_EMAIL_USERS_OPTION_RANGE_MIN', '1');

define( 'EM_EMAIL_USERS_OPTION_RANGE_MAX', '100');

define( 'EM_EMAIL_USERS_OPTION_DELAY_MIN', '1');

define( 'EM_EMAIL_USERS_OPTION_DELAY_MAX', '60');

// Booking status constants

define( 'EM_EMAIL_USERS_STATUS_PENDING', '0');

define( 'EM_EMAIL_USERS_STATUS_CONFIRMED', '1');

define( 'EM_EMAIL_USERS_STATUS_REJECTED', '2');

define( 'EM_EMAIL_USERS_STATUS_CANCELLED', '3');

define( 'EM_EMAIL_USERS_STATUS_WAIT_ON_PAYMENT', '4');

define( 'EM_EMAIL_USERS_STATUS_WAIT_OFF_PAYMENT', '5');


if ( ! class_exists( "em_email_users" ) ) :

// Single class implementing the plugin

class em_email_users {

	// The single instance of the plugin class

    protected static $_instance = null;
	
	// Defaults settings of the plugin

	protected static $_default_settings = array('service' => 'wp', 'format' => 'text', 'bcc' => 'yes', 'range' => 50, 'sleep' => 'wait', 'delay' => 2, 'pending-booking' => 'on', 'confirmed-booking' => 'on', 'rejected-booking' => null, 'cancelled-booking' => null, 'waiting-online' => null, 'waiting-offline' => null);
	
	// Current EM_Event
	protected $_em_event = null;
	
	// Constructor
	
    public function __construct() {

		// Text domain loading

		add_action( 'plugins_loaded', array( $this, 'em_email_users_load_text_domain' ) );

		// Initializations

		add_action( 'plugins_loaded', array( $this, 'em_email_users_init') );

	}
	
	// Create the single instance of this class
	
    public static function instance() {

        if ( is_null( self::$_instance ) ) {

			self::$_instance = new self();

		}

        return self::$_instance;

    }
	
	// Using __clone method is forbidden

    public function __clone() {

        _doing_it_wrong( __FUNCTION__, __( 'Forbidden !' , EM_EMAIL_USERS_SLUG), EM_EMAIL_USERS_VERSION );

    }

	// Using __wakeup method is forbidden
	
    public function __wakeup() {

        _doing_it_wrong( __FUNCTION__, __( 'Forbidden !' , EM_EMAIL_USERS_SLUG), EM_EMAIL_USERS_VERSION );

    }
	
	// This method abort the plugin activation and warns the admin
	
	public function em_email_users_deactivate() {
 
		$plugin_file = plugin_basename( __FILE__ );
		
		if ( is_plugin_active($plugin_file) ) {

			deactivate_plugins( plugin_basename($plugin_file) );
			
			$error_msg = sprintf("%s<br/><br/>%s<br/><br/><a href='".get_admin_url(null, 'plugins.php')."'>%s</a><br/><br/>", __('Please first install and activate Events Manager plugin !', EM_EMAIL_USERS_SLUG), __('Events Manager Email Users could not be activated.', EM_EMAIL_USERS_SLUG), __('Click here to return to the plugins page.', EM_EMAIL_USERS_SLUG));
			
			wp_die( $error_msg );

		}
		
		return true;
	}

	// Text domain loading
	
    public function em_email_users_load_text_domain() {

        load_plugin_textdomain( EM_EMAIL_USERS_SLUG, false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

    }
	
	// This method hooks the footer of the booking page from Events Manager
	
	public function em_bookings_table_footer($em_booking_table) {
	
		if ($em_booking_table) {
		
			$this->em_email_users_output_list($em_booking_table);	
			
		}
		
		return $em_booking_table;
		
	}
	
	// This method is called by the constructor for initializing the plugin

	public function em_email_users_init() {
	
		// Check if EM is installed
		
		if ( !defined('EM_VERSION') ) {
	
			add_action( 'admin_init', array( $this, 'em_email_users_deactivate' ) );

			return false;

		}
		
		// Loading CSS

		add_action( 'admin_init', array( $this, 'em_email_users_css' ) );

		// Hook setting menu (only for admins)
		
		if (is_admin()) {

			add_action( 'admin_init', array( $this, 'em_email_users_register_options' ) );
			
			add_action( 'admin_menu', array( $this, 'em_email_users_menu' ) );

		}
	
		// Hook booking table display in Events Manager
		
		add_action( 'em_bookings_table_footer', array( $this, 'em_bookings_table_footer' ) );
		
		// Action for wp_cron events
		
		add_action( 'em_email_users_scheduled_wp_sending', array( &$this, 'em_email_users_scheduled_wp_sending' ), 10, 4);
		
		add_action( 'em_email_users_scheduled_em_sending', array( &$this, 'em_email_users_scheduled_em_sending' ), 10, 4 );
	
		return true;

	}
	
	// CSS loading

	public function em_email_users_css() {

		wp_register_style( EM_EMAIL_USERS_SLUG, plugins_url( '/css/style.css' , __FILE__ ) );

		wp_enqueue_style( EM_EMAIL_USERS_SLUG );

		return true;
		
	}
	
	// Adding the plugin settings to the option menu

	public function em_email_users_menu() {
	
		add_options_page( EM_EMAIL_USERS_FULL_NAME, EM_EMAIL_USERS_SHORT_NAME, 'manage_options', EM_EMAIL_USERS_OPTIONS, array(&$this, 'em_email_users_options') );
		
		return true;
		
	}
	
	// Main method for displaying the setting form of this plugin
	
	public function em_email_users_options() {
	
		?>
		
		<div class="wrap">
		
		<h1>Events Manager Email Users</h1>
		
		<h2><?php _e( 'Options', EM_EMAIL_USERS_SLUG ); ?></h2>
		
		<form method="post" action="options.php">

			<?php
			
			settings_fields( EM_EMAIL_USERS_OPTIONS );
			
			do_settings_sections( EM_EMAIL_USERS_OPTIONS );
			
			submit_button();
			
			?>

		</form>
		
		</div>
		
		<?php
		
		return true;
		
	}
	
	// Building the setting form
	
	public function em_email_users_register_options() {

		register_setting( EM_EMAIL_USERS_OPTIONS, EM_EMAIL_USERS_OPTIONS, array(&$this, 'em_email_users_options_validate') );
		
		add_settings_section( 'em-email-users-options-section-config', __('Email configuration :', EM_EMAIL_USERS_SLUG ) , null, EM_EMAIL_USERS_OPTIONS);
		
		add_settings_field('em-email-users-options-service', __('Email service :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_service'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-config');
		
		add_settings_section( 'em-email-users-options-section-wpmail', __('Wordpress email parameters :', EM_EMAIL_USERS_SLUG ) , array(&$this, 'em_email_users_section_wpmail'), EM_EMAIL_USERS_OPTIONS);

		add_settings_field('em-email-users-options-format', __('Email format :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_format'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-wpmail');

		add_settings_field('em-email-users-options-bcc', __('Hide recipients in BCC field :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_bcc'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-wpmail');
		
		add_settings_section( 'em-email-users-options-section-spam', __('Arrangements with spam filters :', EM_EMAIL_USERS_SLUG ) , null, EM_EMAIL_USERS_OPTIONS);

		add_settings_field('em-email-users-options-range', __('Maximum number of recipients per email :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_range'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-spam');

		add_settings_field('em-email-users-options-sleep', __('Strategy for delaying sendings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_sleep'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-spam');
		
		add_settings_field('em-email-users-options-delay', __('Delay between two email sendings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_delay'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-spam');
		
		add_settings_section( 'em-email-users-options-section-include', __('Include into recipients lists :', EM_EMAIL_USERS_SLUG ) , null, EM_EMAIL_USERS_OPTIONS);

		add_settings_field('em-email-users-options-include-pending', __('Pending bookings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_include_pending'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-include');
		
		add_settings_field('em-email-users-options-include-confirmed', __('Confirmed bookings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_include_confirmed'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-include');
		
		add_settings_field('em-email-users-options-include-rejected', __('Rejected bookings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_include_rejected'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-include');
		
		add_settings_field('em-email-users-options-include-cancelled', __('Cancelled bookings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_include_cancelled'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-include');
		
		add_settings_field('em-email-users-options-include-wait-online', __('Awaiting online payment bookings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_include_wait_online'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-include');
		
		add_settings_field('em-email-users-options-include-wait-offline', __('Awaiting offline payment bookings :', EM_EMAIL_USERS_SLUG ), array(&$this, 'em_email_users_options_include_wait_offline'), EM_EMAIL_USERS_OPTIONS, 'em-email-users-options-section-include');

		return true;

	}
	
	// Validating the values from the setting form
	
	public function em_email_users_options_validate($arg) {
	
		$range = (int) ($arg['range']);
		
		if ($range < ((int) EM_EMAIL_USERS_OPTION_RANGE_MIN)) $range = (int) EM_EMAIL_USERS_OPTION_RANGE_MIN;
		
		if ($range > ((int) EM_EMAIL_USERS_OPTION_RANGE_MAX)) $range = (int) EM_EMAIL_USERS_OPTION_RANGE_MAX;
		
		$arg['range'] = $range;

		$delay = (int) ($arg['delay']);
		
		if ($delay < ((int) EM_EMAIL_USERS_OPTION_DELAY_MIN)) $delay = (int) EM_EMAIL_USERS_OPTION_DELAY_MIN;
		
		if ($delay > ((int) EM_EMAIL_USERS_OPTION_DELAY_MAX)) $delay = (int) EM_EMAIL_USERS_OPTION_DELAY_MAX;
		
		$arg['delay'] = $delay;

		return $arg;

	}
	
	// Managing de WP email parameters section
	
	public function em_email_users_section_wpmail() {
	
		?>

		<p class="em-email-users-setting-info"><?php _e( 'These options are considered only if WP email service is selected above.', EM_EMAIL_USERS_SLUG ); ?></p>
		
		<?php
		
		return true;
	
	}
	
	public function em_email_users_options_service() {

		$value = $this->em_email_users_get_option_service();
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-service-em-id" name="em-email-users-options[service]" value="em" <?php checked('em', $value); ?> />
		
		<label for="em-email-users-option-service-em-id"><?php _e( 'Events Manager Plugin email service (em_mailer)', EM_EMAIL_USERS_SLUG ); ?></label>
			
		</p>

		<p class="em-email-users-setting-info"><?php _e( 'Please note that hiding recipients in BCC field is impossible with this service.', EM_EMAIL_USERS_SLUG ); ?></p>
		
		<br/>
		
		<p class="em-email-users-setting-field">

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-service-wp-id" name="em-email-users-options[service]" value="wp" <?php checked('wp', $value); ?> />
		
		<label for="em-email-users-option-service-wp-id"><?php _e( 'WordPress default email service (wp_mail)', EM_EMAIL_USERS_SLUG ); ?></label>

		</p>

		<p class="em-email-users-setting-info"><?php _e( 'If selected, you can configure parameters in the section below.', EM_EMAIL_USERS_SLUG ); ?></p>

		<?php

		return true;
		
	}

	// Managing the format option from the setting form
	
	public function em_email_users_options_format() {
		
		$value = $this->em_email_users_get_option_format();
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-format-text-id" name="em-email-users-options[format]" value="text" <?php checked('text', $value); ?> />
		
		<label for="em-email-users-option-format-text-id"><?php _e( 'Plain text', EM_EMAIL_USERS_SLUG ); ?></label>
	
		<br/>

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-format-html-id" name="em-email-users-options[format]" value="html" <?php checked('html', $value); ?> />
		
		<label for="em-email-users-option-format-html-id"><?php _e( 'HTML', EM_EMAIL_USERS_SLUG ); ?></label>
	
		</p>
			
		<?php
		
		return true;

	}
	
	// Managing the bcc option from the setting form
	
	public function em_email_users_options_bcc() {
		
		$value = $this->em_email_users_get_option_bcc();
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-bcc-no-id" name="em-email-users-options[bcc]" value="no" <?php checked('no', $value); ?> />
		
		<label for="em-email-users-option-bcc-no-id"><?php _e( 'No', EM_EMAIL_USERS_SLUG ); ?></label>
	
		<br/>

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-bcc-yes-id" name="em-email-users-options[bcc]" value="yes" <?php checked('yes', $value); ?> />
		
		<label for="em-email-users-option-bcc-yes-id"><?php _e( 'Yes', EM_EMAIL_USERS_SLUG ); ?></label>
	
		</p>
			
		<?php
		
		return true;

	}
	
	// Managing the range option from the setting form
	
	public function em_email_users_options_range() {
		
		$value = $this->em_email_users_get_option_range();
		
		?>
		
		<p class="em-email-users-setting-field">

		<label for="em-email-users-option-range-id"><?php _e( 'Clip sending by range of :', EM_EMAIL_USERS_SLUG ); ?></label>
		
		<br/>
		
		<input type="number" class="em-email-users-option-field-class" id="em-email-users-option-range-id" name="em-email-users-options[range]" value="<?php echo $value; ?>" min="<?php echo EM_EMAIL_USERS_OPTION_RANGE_MIN; ?>" max="<?php echo EM_EMAIL_USERS_OPTION_RANGE_MAX; ?>" />&nbsp;<?php _e( 'persons', EM_EMAIL_USERS_SLUG ); ?>
	
		</p>
			
		<?php
		
		return true;

	}
	
	// Managing the sleep option from the setting form
	
	public function em_email_users_options_sleep() {
	
		$value = $this->em_email_users_get_option_sleep();
		
		$unit = __( 'seconds', EM_EMAIL_USERS_SLUG );
		
		$wait_unit = (! get_magic_quotes_gpc ()) ? addslashes ($unit) : $unit;
		
		$js_wait = "onchange=\"var u=getElementById('em-email-users-delay-unit-id');if (u) {u.innerHTML='".$wait_unit."';}\"";
		
		$unit = __( 'minutes', EM_EMAIL_USERS_SLUG );
		
		$schedule_unit = (! get_magic_quotes_gpc ()) ? addslashes ($unit) : $unit;
		
		$js_schedule = "onchange=\"var u=getElementById('em-email-users-delay-unit-id');if (u) {u.innerHTML='".$schedule_unit."';}\"";
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-sleep-wait-id" name="em-email-users-options[sleep]" value="wait" <?php checked('wait', $value); ?> <?php echo $js_wait; ?> />
		
		<label for="em-email-users-option-sleep-wait-id"><?php _e( 'Wait', EM_EMAIL_USERS_SLUG ); ?></label>
	
		</p>
		
		<p class="em-email-users-setting-info"><?php _e( 'You will have to wait for complete email sendings before receiving a response from the server (call to PHP sleep function)', EM_EMAIL_USERS_SLUG ); ?></p>
		
		<p class="em-email-users-setting-field">

		<input type="radio" class="em-email-users-option-radio-class" id="em-email-users-option-sleep-cron-id" name="em-email-users-options[sleep]" value="cron" <?php checked('cron', $value); ?>  <?php echo $js_schedule; ?> />
		
		<label for="em-email-users-option-sleep-cron-id"><?php _e( 'Schedule', EM_EMAIL_USERS_SLUG ); ?></label>
	
		</p>
		
		<p class="em-email-users-setting-info"><?php _e( 'Sendings are scheduled by the WordPress cron service (wp_cron) : no need to wait.', EM_EMAIL_USERS_SLUG ); ?></p>

		<p class="em-email-users-setting-info"><?php _e( 'Please note that some issues are sometimes complained with this service.', EM_EMAIL_USERS_SLUG ); ?></p>
			
		<?php
		
		return true;
		
	}

	// Managing the delay option from the setting form

	public function em_email_users_options_delay() {

		$sleep = $this->em_email_users_get_option_sleep();

		$value = $this->em_email_users_get_option_delay();
		
		$unit = (strcmp($sleep, "cron"))?"seconds":"minutes";
		
		?>
		
		<p class="em-email-users-setting-field">

		<label for="em-email-users-option-range-id"><?php _e( 'Wait for :', EM_EMAIL_USERS_SLUG ); ?></label>
		
		<br/>
		
		<input type="number" class="em-email-users-option-field-class" id="em-email-users-option-delay-id" name="em-email-users-options[delay]" value="<?php echo $value; ?>" min="<?php echo EM_EMAIL_USERS_OPTION_DELAY_MIN; ?>" max="<?php echo EM_EMAIL_USERS_OPTION_DELAY_MAX; ?>" />&nbsp;<span id="em-email-users-delay-unit-id"><?php _e( $unit, EM_EMAIL_USERS_SLUG ); ?></span>
	
		</p>
			
		<?php
		
		return true;

	}
	
	// Managing the pending booking option from the setting form
	
	public function em_email_users_options_include_pending() {

		$value = $this->em_email_users_get_option_include(EM_EMAIL_USERS_OPTION_PENDING_NAME);
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="checkbox" class="em-email-users-option-check-class" id="em-email-users-option-pending-id" name="em-email-users-options[<?php echo EM_EMAIL_USERS_OPTION_PENDING_NAME; ?>]" value="on" <?php checked("on", $value); ?> />
	
		</p>
			
		<?php
		
		return true;
	
	}
	
	// Managing the confirmed booking option from the setting form
	
	public function em_email_users_options_include_confirmed() {

		$value = $this->em_email_users_get_option_include(EM_EMAIL_USERS_OPTION_CONFIRMED_NAME);
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="checkbox" class="em-email-users-option-check-class" id="em-email-users-option-confirmed-id" name="em-email-users-options[<?php echo EM_EMAIL_USERS_OPTION_CONFIRMED_NAME; ?>]" value="on" <?php checked("on", $value); ?> />
	
		</p>
			
		<?php

		return true;
	
	}

	// Managing the rejected booking option from the setting form
	
	public function em_email_users_options_include_rejected() {

		$value = $this->em_email_users_get_option_include(EM_EMAIL_USERS_OPTION_REJECTED_NAME);
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="checkbox" class="em-email-users-option-check-class" id="em-email-users-option-rejected-id" name="em-email-users-options[<?php echo EM_EMAIL_USERS_OPTION_REJECTED_NAME; ?>]" value="on" <?php checked("on", $value); ?> />
	
		</p>
			
		<?php

		return true;
	
	}

	// Managing the cancelled booking option from the setting form
	
	public function em_email_users_options_include_cancelled() {

		$value = $this->em_email_users_get_option_include(EM_EMAIL_USERS_OPTION_CANCELLED_NAME);
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="checkbox" class="em-email-users-option-check-class" id="em-email-users-option-cancelled-id" name="em-email-users-options[<?php echo EM_EMAIL_USERS_OPTION_CANCELLED_NAME; ?>]" value="on" <?php checked("on", $value); ?> />
	
		</p>
			
		<?php

		return true;
	
	}
	
	// Managing the awaiting online payment booking option from the setting form
	
	public function em_email_users_options_include_wait_online() {

		$value = $this->em_email_users_get_option_include(EM_EMAIL_USERS_OPTION_WAIT_ON_NAME);
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="checkbox" class="em-email-users-option-check-class" id="em-email-users-option-wait-online-id" name="em-email-users-options[<?php echo EM_EMAIL_USERS_OPTION_WAIT_ON_NAME; ?>]" value="on" <?php checked("on", $value); ?> />
	
		</p>
			
		<?php

		return true;
	
	}
	
	
	// Managing the awaiting offline payment booking option from the setting form
	
	public function em_email_users_options_include_wait_offline() {

		$value = $this->em_email_users_get_option_include(EM_EMAIL_USERS_OPTION_WAIT_OFF_NAME);
		
		?>
		
		<p class="em-email-users-setting-field">

		<input type="checkbox" class="em-email-users-option-check-class" id="em-email-users-option-wait-offline-id" name="em-email-users-options[<?php echo EM_EMAIL_USERS_OPTION_WAIT_OFF_NAME; ?>]" value="on" <?php checked("on", $value); ?> />
	
		</p>
			
		<?php

		return true;
	
	}
	
	// Manages the scheduled events for wp_mail sendings
	
	public function em_email_users_scheduled_wp_sending($to, $subject, $mailtext, $header_slice) {
	
		$ret = @wp_mail($to, $subject, $mailtext, $header_slice);
	
		return $ret;
	
	}

	// Manages the scheduled events for em_mailer sendings
	
	public function em_email_users_scheduled_em_sending($id, $subject, $message, $slice) {
	
		$ret = false;

		if (function_exists('em_get_event')) {
		
			$em_event = em_get_event($id,'event_id');
			
			if ($em_event) {
			
				if ( is_object($em_event) && get_class($em_event) == 'EM_Event' ) {
			
					$em_event->email_send($subject, $message, $slice);
					
				}
			
			}
		
		}
	
		return true;
	
	}
	
	// Getter method for service option
	
	protected function em_email_users_get_option_service() {

		$options = get_option(EM_EMAIL_USERS_OPTIONS, self::$_default_settings);
		
		$value = (isset($options['service']))?$options['service']:self::$_default_settings['service'];
		
		return $value;
		
	}
	
	// Getter method for format option
	
	protected function em_email_users_get_option_format() {

		$options = get_option(EM_EMAIL_USERS_OPTIONS, self::$_default_settings);
		
		$value = (isset($options['format']))?$options['format']:self::$_default_settings['format'];
		
		return $value;
		
	}
	
	// Getter method for sleep option
	
	protected function em_email_users_get_option_sleep() {

		$options = get_option(EM_EMAIL_USERS_OPTIONS, self::$_default_settings);
		
		$value = (isset($options['sleep']))?$options['sleep']:self::$_default_settings['sleep'];
		
		return $value;
		
	}
	
	// Special getter for the email form 
	
	protected function em_email_users_is_html() {

		$service = $this->em_email_users_get_option_service();
		
		if (strcmp($service, "wp")) {
	
			if (function_exists('get_option')) {
			
				$ret = (get_option('dbem_smtp_html'))?true:false;
				
			}
			
			else {
			
				$ret = false;
			
			}
		
		}
		
		else {
		
			$format = $this->em_email_users_get_option_format();
			
			$ret = (strcmp($format, "html"))?false:true;
		
		}
		
		return $ret;
		
	}
	
	// Getter method for bcc option

	protected function em_email_users_get_option_bcc() {

		$options = get_option(EM_EMAIL_USERS_OPTIONS, self::$_default_settings);

		$value = (isset($options['bcc']))?$options['bcc']:self::$_default_settings['bcc'];
		
		return $value;
		
	}
		
	// Getter method for range option
	
	protected function em_email_users_get_option_range() {

		$options = get_option(EM_EMAIL_USERS_OPTIONS, self::$_default_settings);

		$value = (int) (isset($options['range']))?$options['range']:self::$_default_settings['range'];
		
		return $value;
		
	}

	// Getter method for delay option
	
	protected function em_email_users_get_option_delay() {

		$options = get_option(EM_EMAIL_USERS_OPTIONS, self::$_default_settings);

		$value = (int) (isset($options['delay']))?$options['delay']:self::$_default_settings['delay'];
		
		return $value;
		
	}

	// Getter method for booking status including option
	
	protected function em_email_users_get_option_include($status) {
	
		$options = get_option(EM_EMAIL_USERS_OPTIONS, self::$_default_settings);

		$value = (isset($options[$status]))?$options[$status]:null;
		
		return $value;
		
	}

	// Displays the sending mail form or the result of this sending
	
	protected function em_email_users_output_list(&$em_booking_table) {

		$save_limit = $em_booking_table->limit;

		$em_booking_table->limit = intval(EM_EMAIL_USERS_OPTION_RANGE_MAX);		

		$em_booking_table->get_bookings(true);
		
		$em_booking_table->limit = $save_limit;
		
		$em_event = $em_booking_table->get_event();
		
		if ($em_event !== false) {
		
			$this->_em_event = $em_event;

			?>
			
			<div class='em-email-users-container' id="em-email-users-list-id">

			<h2><?php _e( 'Send email to users', EM_EMAIL_USERS_SLUG ); ?></h2>
			
			<?php

			if (isset($_POST['em-email-users-submit'])) {
			
				$this->em_email_users_send();
		
			}
			else {
			
				$this->em_email_users_display($em_booking_table);
			
			}
			
			?></div><?php 
			
		}
		
		return $em_booking_table;
		
	}
	
	// Builds the recipients list then displays the sending email form
	
	protected function em_email_users_display(&$em_booking_table) {
		
		if ( ($em_booking_table) && ($this->_em_event) ) {
		
			$format = $this->em_email_users_get_option_format();
			
			$bcc = $this->em_email_users_get_option_bcc();
		
			$event_name = $this->_em_event->event_name;
			
			$email_tab = array();
			
			$email_list = "";
			
			foreach ($em_booking_table->bookings->bookings as $em_booking) {
			
				$status = (int) $em_booking->booking_status;
				
				$option_name = $this->status_to_option_name($status);
				
				if (strlen($option_name) > 0) {
				
					$option_value = $this->em_email_users_get_option_include($option_name);
				
					if ( !(strcmp($option_value, 'on') ) ) {

						$email_tab[] = $em_booking->get_person()->user_email;
						
					}
					
				}
				
			}
			
			$email_list = implode(", ", array_unique($email_tab) );
			
			$this->em_email_users_message_form(trim($email_list), $event_name, $format, $bcc);
			
		}
		
		return true;

	}
	
	// Displays the email sending form if the recipient list is not empty
	
	protected function em_email_users_message_form($email_list, $event_name, $format, $bcc) {
	
		if (strlen($email_list) == 0 ) {
		
			?><p><?php _e( 'Recipient list is empty.', EM_EMAIL_USERS_SLUG ); ?></p><?php
			
			return false;
		
		}
		
		else {
		
			?>
			
			<p><?php _e( 'Recipients list :', EM_EMAIL_USERS_SLUG ); ?></p>
			
			<p>

			<a href="mailto:<?php echo $email_list; ?>?subject=<?php echo $event_name; ?>" title="<?php _e( 'Open email client with this recipient list', EM_EMAIL_USERS_SLUG ); ?>">
			
			<?php echo $email_list; ?>
			
			</a>
			
			</p>
				
			<form class='em-email-users-form-class' id="em-email-users-form-id" method="post" >
			
				<input type="hidden" value="<?php echo $email_list; ?>" name="em-email-users-recipient-list" />

				<p>
				
				<label for="em-email-users-form-subject-id"><?php _e( 'Subject :', EM_EMAIL_USERS_SLUG ); ?></label>
			
				<br/>

				<input type="text" class="em-email-users-form-field-class" id="em-email-users-form-subject-id" name="em-email-users-subject" value="<?php echo $event_name; ?>" maxlength="80" />
			
				</p>
				
				<?php if ($this->em_email_users_is_html()) {
			
					?><p><?php _e( 'Message :', EM_EMAIL_USERS_SLUG ); ?></p><?php
					
					wp_editor("", "em-email-users-message", array( 'media_buttons' => false, 'textarea_rows' => 8 ) );

				}

				else { ?>
				
					<p>

					<label for="em-email-users-form-message-id"><?php _e( 'Message :', EM_EMAIL_USERS_SLUG ); ?></label>
				
					<br/>

					<textarea class="em-email-users-form-textarea-class" id="em-email-users-form-message-id" name="em-email-users-message" rows="5"></textarea>
					
					</p>
				
				<?php } ?>
				
				<?php if (strcmp($bcc, "yes")) { ?>
		
					<p>

					<input type="checkbox" class="em-email-users-form-checkbox-class" id="em-email-users-form-copy-id" name="em-email-users-copy" />				
					<label for="em-email-users-form-copy-id"><?php _e( 'Send me a copy', EM_EMAIL_USERS_SLUG ); ?></label>
					
					</p>
					
				<?php } ?>
				
				<br/>
				
				<p>

				<input type="submit" class="button button-primary em-email-users-form-button-class" id="em-email-users-form-submit-id" value="<?php _e( 'Send', EM_EMAIL_USERS_SLUG ); ?>" name="em-email-users-submit" />

				</p>
				
			</form>
			
			<?php
			
			return true;
			
		}
		
	}
	
	// Checks the inputs and prepares the email sending

	protected function em_email_users_send() {

		global $current_user;

		get_currentuserinfo();

		$current_url = $_SERVER['REQUEST_URI'];
		
		$current_user_name = (strlen($current_user->display_name) > 0)?$current_user->display_name:$current_user->user_login;
		
		$current_user_email = $current_user->user_email;
		
		$recipients = stripslashes(trim($_POST['em-email-users-recipient-list']));
		
		$subject = stripslashes(trim($_POST['em-email-users-subject']));
		
		$message = stripslashes(trim($_POST['em-email-users-message']));

		if ( (strlen($current_user_name) == 0) || (strlen($current_user_email) == 0) ) {

			$this->em_email_users_send_error("Could not retrieve current user infos.", $current_url);
			
			return false;

		}

		elseif (strlen($recipients) == 0) {
		
			$this->em_email_users_send_error("Recipient list is empty.", $current_url);
			
			return false;

		}

		elseif (strlen($subject) == 0) {

			$this->em_email_users_send_error("Subject is empty.", $current_url);
			
			return false;

		}

		elseif (strlen($message) == 0) {

			$this->em_email_users_send_error("Message is empty.", $current_url);
			
			return false;

		}

		$copy_parameter = (isset($_POST['em-email-users-copy']))?$_POST['em-email-users-copy']:null;
		
		$send_copy = (strlen(stripslashes(trim($copy_parameter))) == 0)?false:true;
		
		?><div class="em-email-users-sending-info"><?php
		
		$ret = $this->em_email_users_email($current_user_name, $current_user_email, $recipients, $subject, $message, $send_copy);
			
		?></div><?php
		
		if ($ret) {

			$this->em_email_users_send_success($current_url);

		}
		else {

			$this->em_email_users_send_error("An error occurred while sending email...", $current_url);

		}
		
		return $ret;

	}
		
	// Displays the success of the email sending
	
	protected function em_email_users_send_success($return_url) {

		?>
		
		<p class="em-email-users-send-success"><?php _e( 'Your email has been correctly sent.', EM_EMAIL_USERS_SLUG ); ?></p>
		
		<p><a href="<?php echo $return_url; ?>"><?php _e( 'Click here to send another email.', EM_EMAIL_USERS_SLUG ); ?></a></p>
		
		<?php
	
	}
		
	// Displays the failure of the email sending
	
	protected function em_email_users_send_error($message, $return_url) {
	
		?>
		
		<p class="em-email-users-send-error"><?php _e( $message, EM_EMAIL_USERS_SLUG ); ?></p>

		<p>
		
		<a href="<?php echo $return_url; ?>"><?php _e( 'Click here to retry.' , EM_EMAIL_USERS_SLUG ); ?></a>
		
		</p>
		
		<?php

		return true;
	
	}

	// Selects the emailing service then calls the right method
	
	protected function em_email_users_email($sender_name, $sender_email, $recipients, $subject, $message, $copy_to_sender) {
	
		// Without event instance, impossible to send email
	
		if ( ! ($this->_em_event) ) {
		
			return false;
			
		}

		$service = $this->em_email_users_get_option_service();
		
		$is_html = $this->em_email_users_is_html();

		$recipients_tab = explode(", ", $recipients);
		
		$subject_with_placeholders = $this->_em_event->output($subject);

		$msg_with_placeholders = $this->_em_event->output($message);
		
		if ( ! ($is_html) ) {

			$msg_with_placeholders = html_entity_decode(wp_kses_data($msg_with_placeholders));
		
		}
		
		if (strcmp($service, "wp")) {
		
			$ret = $this->em_email_users_em_emailer($sender_email, $recipients_tab, $subject_with_placeholders, $msg_with_placeholders, $copy_to_sender);

		}
		
		else {
		
			$subject_with_placeholders = html_entity_decode(wp_kses_data($subject_with_placeholders));
		
			$ret = $this->em_email_users_wp_email($sender_name, $sender_email, $recipients_tab, $subject_with_placeholders, $msg_with_placeholders, $copy_to_sender);
			
		}
		
		return $ret;
	}
	

	// Manages the sending of the email with EM_Emailer

	protected function em_email_users_em_emailer($sender_email, $recipients_tab, $subject, $message, $copy_to_sender) {

		// Get options
		
		$range = $this->em_email_users_get_option_range();
		
		$sleep = $this->em_email_users_get_option_sleep();
		
		$delay = $this->em_email_users_get_option_delay();
		
		$offset = 0;
		
		$ret = true;
		
		while ($slice = array_slice($recipients_tab, $offset, $range)) {
		
			// CC field is not available with EM_Emailer so the address is added to recipients
			
			if ($copy_to_sender) {
			
				$slice[] = $sender_email;
			
			}
								
			flush();
			
			?>	
			
			<p><?php printf("%s %d %s %d...", __( 'Sending message to recipient(s)', EM_EMAIL_USERS_SLUG ), ((int) $offset + 1), __( 'to', EM_EMAIL_USERS_SLUG ), min(count($recipients_tab),((int) $offset + $range))); ?></p><?php
			
			flush();
			
			// Check if email sending must be scheduled
			if ( ($offset > 0) && (strcmp($sleep, "wait")) ) {
			
				$timing = ((int) ($offset / $range)) * $delay;
				
				$id = $this->_em_event->ID;
			
				$ret_mail = $this->em_email_users_schedule_em_mail($timing, $id, $subject, $message, $slice);
				
			}
			
			else {
			
				$ret_mail = $this->_em_event->email_send($subject, $message, $slice);
			
			}
			
			if ($ret_mail) {
			
				?><p class="em-email-users-send-success"><?php _e( 'Succeeded', EM_EMAIL_USERS_SLUG ); ?></p><?php
			}
			
			else {
			
				?><p class="em-email-users-send-error"><?php _e( 'Failed', EM_EMAIL_USERS_SLUG ); ?></p><?php
				
				$ret = false;

			}
				
			flush();
			
			// Next slice
			
			$offset += $range;
			
			// Delay if a next slice exists
			
			if ($offset < count($recipients_tab)) {
			
				// Call to sleep function if option is set
				
				if (strcmp($sleep, "cron")) {
			
					sleep($delay);
					
				}
				
			}

		}
	
		return $ret;
	
	}
	
	
	// Add a new wp_mail sending in wp_cron table
	
	protected function em_email_users_schedule_em_mail($timing, $id, $subject, $message, $slice) {
	
		// Multiply timing by 60 because the unit for duration is minutes rather than seconds
		
		$cron_time = time() + (((int) $timing) * 60);
	
		wp_schedule_single_event($cron_time, 'em_email_users_scheduled_em_sending', array($id, $subject, $message, $slice) );
	
		return true;
	
	}
	
	// Manages the sending of the email with WP_mail

	protected function em_email_users_wp_email($sender_name, $sender_email, $recipients_tab, $subject, $message, $copy_to_sender) {
	
		// Get options
		
		$format = $this->em_email_users_get_option_format();

		$bcc = $this->em_email_users_get_option_bcc();
		
		$range = $this->em_email_users_get_option_range();
		
		$sleep = $this->em_email_users_get_option_sleep();
		
		$delay = $this->em_email_users_get_option_delay();
		
		// Build wp_mail parameters

		$headers = array();
		
		// Sender
		
		$headers[] = sprintf('From: "%s" <%s>', $sender_name, $sender_email);

		$headers[] = sprintf('Return-Path: <%s>', $sender_email);

		$headers[] = sprintf('Reply-To: "%s" <%s>', $sender_name, $sender_email);
		
		// Email format
		
		$headers[] = sprintf('X-Mailer: PHP %s', phpversion()) ;
		
		$headers[] = 'MIME-Version: 1.0';

		if (strcmp($format, "html")) {

			$headers[] = sprintf('Content-Type: text/plain; charset="%s"', get_bloginfo('charset')) ;

			$message = preg_replace('|&[^a][^m][^p].{0,3};|', '', $message);

			$message = preg_replace('|&amp;|', '&', $message);

			$mailtext = wordwrap(strip_tags($message), 80, "\n");
		
		}
		
		else {
		
			$headers[] = sprintf('Content-Type: %s; charset="%s"', get_bloginfo('html_type'), get_bloginfo('charset')) ;

			$mailtext = "<html><head><title>" . $subject . "</title></head><body>" . nl2br($message) . "</body></html>";
		
		}
		
		// Recipients (hidden or not)
		
		$offset = 0;
		
		$ret = true;
		
		while ($slice = array_slice($recipients_tab, $offset, $range)) {
		
			$list = implode(", ", $slice);
			
			$header_slice = $headers;
		
			if (strcmp($bcc, "yes")) {
			
				$to = $list;
				
				if ($copy_to_sender) {
				
					$header_slice[] = sprintf('Cc: %s', $sender_email);
				
				}
			
			}
			
			else {
			
				$to = sprintf('"%s" <%s>', $sender_name, $sender_email);
				
				$header_slice[] = sprintf('Bcc: %s', $list);
			
			}
								
			flush();
			
			?>	
			
			<p><?php printf("%s %d %s %d...", __( 'Sending message to recipient(s)', EM_EMAIL_USERS_SLUG ), ((int) $offset + 1), __( 'to', EM_EMAIL_USERS_SLUG ), min(count($recipients_tab),((int) $offset + $range))); ?></p><?php
			
			flush();
			
			// Check if email sending must be scheduled
			if ( ($offset > 0) && (strcmp($sleep, "wait")) ) {
			
				$timing = ((int) ($offset / $range)) * $delay;
			
				$ret_mail = $this->em_email_users_schedule_wp_mail($timing, $to, $subject, $mailtext, $header_slice);
				
			}
			
			else {
			
				$ret_mail = @wp_mail($to, $subject, $mailtext, $header_slice);
			
			}
			
			if ($ret_mail) {
			
				?><p class="em-email-users-send-success"><?php _e( 'Succeeded', EM_EMAIL_USERS_SLUG ); ?></p><?php
			}
			
			else {
			
				?><p class="em-email-users-send-error"><?php _e( 'Failed', EM_EMAIL_USERS_SLUG ); ?></p><?php
				
				$ret = false;

			}
				
			flush();
			
			// Next slice
			
			$offset += $range;
			
			// Delay if a next slice exists
			
			if ($offset < count($recipients_tab)) {
			
				// Call to sleep function if option is set
				
				if (strcmp($sleep, "cron")) {
			
					sleep($delay);
					
				}
				
			}

		}
		
		return $ret;

	}
	
	// Add a new wp_mail sending in wp_cron table
	
	protected function em_email_users_schedule_wp_mail($timing, $to, $subject, $mailtext, $header_slice) {
	
		$cron_time = time() + ((int) $timing);
	
		wp_schedule_single_event($cron_time, 'em_email_users_scheduled_wp_sending', array($to, $subject, $mailtext, $header_slice) );
	
		return true;
	
	}

	// Returns the option name for a booking status from Events Manager plugin
	
	protected function status_to_option_name($status) {
	
		$ret = null;
	
		if ($status == (int) EM_EMAIL_USERS_STATUS_PENDING) {
		
			$ret = EM_EMAIL_USERS_OPTION_PENDING_NAME;
		
		}
		
		elseif ($status == (int) EM_EMAIL_USERS_STATUS_CONFIRMED) {

			$ret = EM_EMAIL_USERS_OPTION_CONFIRMED_NAME;
		
		}
		
		elseif ($status == (int) EM_EMAIL_USERS_STATUS_REJECTED) {

			$ret = EM_EMAIL_USERS_OPTION_REJECTED_NAME;
		
		}
		
		elseif ($status == (int) EM_EMAIL_USERS_STATUS_CANCELLED) {

			$ret = EM_EMAIL_USERS_OPTION_CANCELLED_NAME;
		
		}
		
		elseif ($status == (int) EM_EMAIL_USERS_STATUS_WAIT_ON_PAYMENT) {

			$ret = EM_EMAIL_USERS_OPTION_WAIT_ON_NAME;
		
		}
		
		elseif ($status == (int) EM_EMAIL_USERS_STATUS_WAIT_OFF_PAYMENT) {

			$ret = EM_EMAIL_USERS_OPTION_WAIT_OFF_NAME;
		
		}

		return $ret;
	}
	
}

endif; // class_exists

function em_email_users() {

	return em_email_users::instance();

}

// Compatibility

$GLOBALS['em_email_users'] = em_email_users();

?>
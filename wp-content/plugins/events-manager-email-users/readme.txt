=== Events Manager Email Users ===
Contributors: PetiLabo
Tags: Events Manager, Email, Contact List
Donate link: Leave a comment on http://www.grolabo.net !
Requires at least: 3.5
Tested up to: 4.0
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

An addon for Events Manager plugin, offering email facilities in order to contact the users who have booked an event.

== Description ==

When you are using the Events Manager plugin on your WordPress website, you sometimes need to contact the users who have booked a given event. This feature is not available within the current version of the Events Manager plugin : that's the job done by this little addon !

When this plugin is installed and activated, you can manage the following options via the settings menu :

* Email service : Default WordPress email service (wp_mail) or Events Manager plugin email service (EM_Mailer class)

* Email format : plain text or HTML

* Hide recipients : uses "To:" field (visible) or "Bcc:" field (hidden)

* Max number of recipients per email : sends a new email for each range of recipients list (attempt to avoid being considered spammy)

* Delay between two email sendings : time elapsed before sending a new email (attempt to avoid being considered spammy)

* Delaying strategy between two sendings : really waits (by calling a server-side sleep function) or schedules the sending (via wp_cron function)

* Recipients to include in the list : depends on the user's booking status for the given event : pending, confirmed, rejected or cancelled.

You can access the email form via the booking managing page handled by the Events Manager plugin. Select an event : you'll find the "Send email to users" section at the bottom of the page.

If you prefer to use your own email client, the recipients list is displayed as a "mailto:" link, with the event name as default subject.

Available languages :

* English

* French

The plugin is ready to translate, all texts are defined in the POT file.

== Installation ==

1. First install and activate the Events Manager plugin, which is the core plugin for this addon.

2. Then go to Plugins > Add New in the admin area, and search for Events Manager Email Users.

3. Click install, once installed, activate and you're done!

== Frequently Asked Questions ==

= What about the risk for sent emails to be considered spammy ?

Obviously, the risk exists ! This plugin helps with some features :

* Defines a maximum size for recipients lists. For example, if you set this max size to 20 and have to send email to 30 addresses, the plugin will send an email for the 20 first addresses, then wait some seconds, then send a new email for the 10 last addresses.

* Delay between two sendings, with two available strategies :

  - The server really waits (by call to sleep function) : the waiting time can be defined between 1 and 60 seconds.
    Note : during the email sendings and the waiting times, you won't receive infos into the admin panel. The infos will be displayed only when all is definetely done.
  
  - The sendings are scheduled (using the wp_cron service) : the scheduling time can be defined between 1 and 60 minutes.
    Note : if your website is very little visited, the sendings may take longer than planned (well known issue for wp_cron).

For more safety, you may also use SMTP protocol.

= How can I send emails via SMTP ?

It depends on the email service you selected in the setting menu : 

* WordPress default email service : you can install for example the WP Mail SMTP plugin (http://wordpress.org/plugins/wp-mail-smtp/), which reconfigures the wp_mail() function to use SMTP instead of mail().

* Events Manager email service : you can choose the SMTP protocol in the settings of Events Manager plugin.

= Is there a maximum number of recipients ?

Theoretically, no. But practically, if you manage hundreds or thousands of bookings for your events, you may encounter problems with the spam filters of your users or customers. That's why I intend to develop soon a new release of this plugin, able to work with the MailChimp API, and automatically create MailChimp campaigns.

= Is it possible to use Events Manager placeholders into subject or message ?

Yes it is. But pay attention with placeholders generating HTML : if you are using one of them, you may select HTML format in the plugin settings page. Please also note that for the moment, individual booking informations (eg #_BOOKINGID) are not managed, as the same message is sent to all users for a given event.

== Screenshots ==

1. The email sending form as displayed in the event booking management page (in french)

2. The top of the setting page of the plugin (in french)

3. The bottom of the setting page of the plugin (in french)

== Changelog ==

= 1.2.3 =

* Bug fix : the recipient list is no longer limited to the default 20 rows view for reservations

= 1.2.2 =

* Bug fix : awaiting online and offline payment bookings are now managed

= 1.2.1 =

* Now translates placeholders relating to the event

= 1.2 =

* New feature : choice between two delaying strategies (sleep or wp_cron)

= 1.1 =

* Minor fix in the option getter methods
* New feature : ability to use the mailing service powered by Events Manager Plugin (EM_Mailer class)

= 1.0 =

* Initial plugin release.

== Upgrade Notice ==

= 1.2.3 =

* Bug fix : the recipient list is no longer limited to the default 20 rows view for reservations

= 1.2.2 =

* Bug fix : awaiting online and offline payment bookings are now managed

= 1.2.1 =

* Now translates placeholders relating to the event

= 1.2 =

* New feature : choice between two delaying strategies (sleep or wp_cron)

= 1.1 =

* Minor fix in the option getter methods
* New feature : ability to use the mailing service powered by Events Manager Plugin (EM_Mailer class)

= 1.0 =

* Let's go !
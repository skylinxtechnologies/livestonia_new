<?php
/**
* Plugin Name: 	Posts Block Ordering
* Plugin URI:  	https://drive.google.com/open?id=0B3RmFEM0zy_2NGk3Q21BTlM3bDA
* Description:  The plugin offers settings' page for changing the order of posts in front page
* Version:		1.0
* Author:		Muhammad Waseem
* Author URI:	https://www.facebook.com/engineer.m.waseem
**/

/**
 *
 * Do not execute this class outside WordPress
 *
 */

if ( ! class_exists( 'WP' ) ) 
{
	die();
}

/**
 *
 * PO_Block
 * For displaying and handling post block filters 
 *
 */
abstract class PO_Block
{

	static private $po_setting_key 			= 'po_block_settings';
	static private $po_setting;
	static private $po_plugin_settings_tabs	= array();
	static private $po_plugin_options_key	= 'po_plugin_options';

	public static function load()
	{
		add_action('init', array(self::class, 'loadTextDomain'));
		add_action('init', array(self::class, 'loadPluginSettings'));
		add_action('admin_init', array(self::class, 'registerPlugin'));
		add_action('admin_menu', array(self::class, 'registerPluginMenu'));
	}

	public static function loadTextDomain()
	{
		load_plugin_textdomain('po-block', false, dirname(plugin_basename(__FILE__)).'/languages');
	}

	public static function loadPluginSettings()
	{
		self::$po_setting = (array) get_option( self::$po_setting_key );
	}

	public static function registerPlugin()
	{
		self::$po_plugin_settings_tabs[ self::$po_setting_key ] = __('Post Block', 'po-block');
		register_setting(self::$po_setting_key, self::$po_setting_key);
		add_settings_section('po_section', __('', 'po-block'), array(self::class, 'poSectionContent'), self::$po_setting_key);
		add_settings_field('po_post_block', __('Post Block Options', 'po-block'), array(self::class, 'postCountField'), self::$po_setting_key, 'po_section');
	}

	public static function poSectionContent()
	{
		echo '<p><em>Following fields are filters for posts blocks in front page.</em></p>';
	}

	public static function postCountField()
	{
		$po_post_block = (isset(self::$po_setting['po_post_block']) ? (self::$po_setting['po_post_block']) : '');
        $fields = array('post_order' => 'Post order', 'no_of_posts_to_show' => 'No. of Posts to show');
        ?>
        <ul>
            <li>
                <input type="number" id="no_of_posts_to_show" name="<?php echo self::$po_setting_key; ?>[po_post_block][no_of_posts_to_show]" value="<?php echo(!empty($po_post_block['no_of_posts_to_show']) ?  $po_post_block['no_of_posts_to_show'] : 2); ?>">
                <label for="no_of_posts_to_show"><em>Number of posts to show. Minimum value is 2.</em></label>
            </li>
            <li>
                <select name="<?php echo self::$po_setting_key; ?>[po_post_block][post_order]" id="post_order">
                	<?php if($po_post_block['post_order'] == 'asc'):?>
                		<option value="asc" selected>Ascending</option>
                		<option value="desc">Descending</option>
                	<?php else: ?>
                		<option value="asc">Ascending</option>
                		<option value="desc" selected>Descending</option>
                	<?php endif; ?>
                </select>
                <label for="post_order"><em>Order of posts. Descending by default.</em></label>
            </li>
        </ul>
        <?php
	}

	public static function registerPluginMenu()
	{
		add_options_page(__('Post Block Filter', 'po-block'), __('Post Block Filter', 'po-block'), 'manage_options', self::$po_plugin_options_key, array(self::class, 'pluginPage'));
	}

	public static function pluginPage()
	{
		$tab = isset($_GET['tab']) ? $_GET['tab'] : self::$po_setting_key;
		?>
		<div class="wrap">
			<?php self::pluginTabs(); ?>
			<form action="options.php" method="post" accept-charset="utf-8">
				<?php settings_fields($tab); ?>
				<?php do_settings_sections($tab); ?>
				<?php submit_button(); ?>
			</form>
		</div>
		<?php
	}

	public static function pluginTabs()
	{
		$current_tab = isset($_GET['tab']) ? $_GET['tab'] : self::$po_setting_key;
        get_screen_icon();
        echo '<h2 class="nav-tab-wrapper">';
        foreach (self::$po_plugin_settings_tabs as $key => $value) {
            $active = $current_tab == $key ? 'nav-tab-active' : '';
            echo '<a class="nav-tab ' . $active . '" href="?page=' . self::$po_plugin_options_key . '&tab=' . $key . '">' . $value . '</a>';
        }
        echo '</h2>';
	}
}
add_action('plugins_loaded', array('PO_Block', 'load'));
//plugin scripting
jQuery(document).ready(function($){
	
	var value = $("#enp-meta-box-newsletters").val();
	var link  = global_post_preview_link;


	var new_link = link + '&' + 'news=' + value;

	$("#enp-meta-box-submit").attr('href', new_link);
	//console.log(new_link);

	$("#enp-meta-box-newsletters").change(function(event) {
		var target = event.target;
		var new_link = link + '&' + 'news=' + $(target).val();
		$("#enp-meta-box-submit").attr('href', new_link);
		//console.log(new_link);
	});


});
<?php
/**
* Plugin Name: 	Email Notification Preview Meta Box
* Plugin URI:  	https://drive.google.com/open?id=0B3RmFEM0zy_2enZoMDNFNC1mcmc
* Description:  This plugin shows meta box in wordpress add/edit post page to preview email notifications before sending emails
* Version:		1.0
* Author:		Muhammad Waseem
* Author URI:	https://www.facebook.com/engineer.m.waseem
**/

if ( ! class_exists( 'WP' ) ) 
{
	die();
}
/**
* The class which controls the plugin
* init at 'Plugins_loaded' hook
*/

class ENP_Meta_Box
{

	public static function init()
	{
		add_action('init', array(__CLASS__, 'load_textdomain'));
		add_action('admin_print_styles', array(__CLASS__, 'enqueue_styles'));
		add_action('admin_print_scripts', array(__CLASS__, 'enqueue_scripts'));
		add_action('load-post.php', array(__CLASS__, 'unregister_meta_box_on_post_publish'), 10, 1);
		add_action('add_meta_boxes', array(__CLASS__, 'register_meta_box'));

	}
	public static function load_textdomain()
	{
		return load_plugin_textdomain('email-notification-preview-meta-box');
	}
	public static function register_meta_box()
	{
		add_meta_box('enp_preview_meta_box', 'Preview Newsletters', array(__CLASS__, 'enp_preview_meta_box_markup'), 'post', 'side', 'high');
	}
	public static function enp_preview_meta_box_markup()
	{
		wp_nonce_field( basename(__FILE__), 'enp-meta-box-nonce' );
		global $wpdb;
		$table = $wpdb->prefix . 'es_templatetable';
		$results = $wpdb->get_results("SELECT * FROM $table");
		?>
		<div class="enp-meta-box-field">
			<label for="enp-meta-box-newsletters">Available Newsletters</label>
			<select name="enp-meta-box-newsletters" id="enp-meta-box-newsletters">
				<?php
				foreach($results as $result)
				{
					echo "<option value='".$result->es_templ_id."'>".$result->es_templ_heading."</option>";
				}
				?>
			</select>
		</div>
		<div class="enp-meta-box-field">
			<a target="_blank" href="<?php echo get_preview_post_link(); ?>&view=newsletter" class="preview button" id="enp-meta-box-submit">Preview Newsletter</a>
		</div>

		<script>
			var global_post_preview_link = '<?php echo get_preview_post_link() . '&view=newsletter'; ?>';
		</script>
		<?php
	}
	public static function enqueue_styles()
	{
		global $typenow;
	    if( $typenow == 'post' ) {
	        wp_enqueue_style( 'enp_meta_box_styles', plugin_dir_url( __FILE__ ) . 'enp-meta-box-style.css' );
	    }
	}
	public static function enqueue_scripts()
	{
		global $typenow;
	    if( $typenow == 'post' ) {
	        wp_enqueue_script( 'enp_meta_box_scripts', plugin_dir_url( __FILE__ ) . 'enp-meta-box-script.js', array('jquery'), '1.0', true );
	    }
	}
	public static function uninstall()
	{

	}
	public static function unregister_meta_box_on_post_publish($wpsc_add_help_tabs)
	{	
		if( isset($_GET['post']) && get_post_status($_GET['post']) == 'publish'){
			echo '<style>
			#enp_preview_meta_box.postbox{display:none;} 
			label[for="enp_preview_meta_box-hide"]{display:none !important;}
			</style>';
		}
		
	}
}

add_action('plugins_loaded', array('ENP_Meta_Box', 'init'));

register_uninstall_hook(__FILE__, array('ENP_Meta_Box', 'uninstall'));
<?php
/**
* Plugin Name: 	stats-plugin
* Plugin URI:  	https://livestonia
* Description:  The plugin offers stats of post types
* Version:		1.0
* Author:		Skylinx Technologies
* Author URI:	https://www.skylinxtech.com
**/

add_action( 'admin_menu','mystatsplugin_admin_actions');

function mystatsplugin_admin_actions(){

	global $current_user;
	get_currentuserinfo(); 

	add_menu_page( 'Statistics Page', 'Statistics Page', $current_user->roles[key($current_user->roles)], 'Statistics', 'mystatsplugin_admin');
}

function mystatsplugin_admin(){
	?>
	<?php
	if ( current_user_can( 'administrator' ) ) {
		?>
		<div class="row">

			<div class="update-nag" align="center" style="" ><h2>Total Users</h2>

				<?php
				$result = count_users();
				echo  $result['total_users'];
				?>
			</div>

			<?php
		}
		?>


		<?php
		if ( current_user_can( 'administrator' ) ) {
			?>
			<div class="update-nag" align="center">
				<h2>Total Events</h2>

				<?php
				global $wpdb;
				$table_name = $wpdb->prefix . "em_events";
				$event_scount = $wpdb->query("SELECT * FROM ".$table_name );
				echo $event_scount;
				?> 
			</div>	
			<?php	
		}
		?>
	</div>

	<?php
	if ( current_user_can( 'administrator' ) ) {
		?>
		<div class="wrap floating-box1">
			<table class="wp-list-table widefat fixed striped posts">
				<caption> <h1>Total Post </h1></caption>
				<thead>
					<tr>
						<th>Name</th>
						<th>Posts</th>
					</tr>
				</thead>
				<tbody>
					<?php $published_posts = wp_count_posts(); 
					foreach ($published_posts as $name => $total) 

					{
						if($total != 0 ){
						?>	
						<tr>
							<td class="title column-title has-row-actions column-primary page-title"><?php echo $name;?></td>
							<td class="title column-title has-row-actions column-primary page-title"><?php echo $total;?></td>
						</tr>

						<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>	
		<?php
	}
	?>
	<div class="heading"> <h1> Blogs</h1></div>
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<th>Image</th>
				<th>Title</th>
				<th>Author</th>
				<th>Actions</th>	
				<th>Status</th>	
			</tr>
		</thead>	
		<tbody>

			<?php
			global $wpdb;
			global $post;
			global $current_user; 

			get_currentuserinfo(); 
			$table = $wpdb->prefix . "posts";
			$str = "SELECT $wpdb->posts.* FROM ".$table." WHERE post_type = 'post' ";


			$result = $wpdb->get_results($str);
			if ( current_user_can( 'administrator' ) ) {
				foreach($result as $post){
					setup_postdata($post);
					?>

					<tr>
						<td><?php the_post_thumbnail(array('height'=>70,'width'=>50));?></td>
						<td><span><a href="<?php the_permalink();?>"><?php the_title();?></span></td>
						<td><span><?php the_author();?></span></td>
						<td><?php edit_post_link(__('Edit Post')); ?></td>
						<td><?php echo get_post_status() ?></td>
					</tr>
					<?php 
				}
			}
			else
			{
				foreach($result as $post){
					setup_postdata($post);
					if(get_the_author_id() == $current_user->ID){
						?>
						<tr>
							<td><?php the_post_thumbnail(array('height'=>70,'width'=>50));?></td>
							<td><span><a href="<?php the_permalink();?>"><?php the_title();?></span></td>
							<td><span><?php the_author();?></span></td>
							<td><?php edit_post_link(__('Edit Post')); ?></td>
							<td><?php echo get_post_status() ?></td>

						</tr>
						<?php 
					}
				}
			}
			?>
		</tbody>
	</table>

	<div class="heading"> <h1> Events</h1></div>

	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<th>Event Name</th>
				<th>Even start Date</th>
				<th>Even end Date</th>
				<th>Action</th>
			</tr>
		</thead>	
		<tbody>


			<?php

			if ( current_user_can( 'administrator' ) ){
				global $wpdb;
				global $post;

				$post_table = $wpdb->prefix ."posts";
				$table = $wpdb->prefix ."em_events";


				$str = "SELECT * FROM ".$post_table." 
				INNER JOIN ".$table." ON ".$post_table.".ID= ".$table.".post_id
				WHERE post_type='event' " ;

				$result = $wpdb->get_results($str);

				foreach($result as $events){
					$post=$events;
					setup_postdata($post);
					?>
					<tr>
						<td><?php echo the_title(); ?>	</td>
						<td><?php echo $events->event_start_date ?></td>
						<td><?php echo $events->event_end_date ?></td>					
						<td><a href="<?php echo get_site_url()?>/wp-admin/post.php?post=<?php echo $events->post_id?>&action=edit">Edit	post</td></a>
					</tr>
					<?php 
				}
			}
			else
			{
				global $wpdb;
				global $post;
				global $current_user; 
				get_currentuserinfo();
				$post_table = $wpdb->prefix . "posts";
				$table = $wpdb->prefix . "em_events";


				$str = "SELECT * FROM ".$post_table." 
				INNER JOIN ".$table." ON ".$post_table.".ID= ".$table.".post_id
				WHERE post_type='event' AND event_owner=".$current_user->ID ;

				$result = $wpdb->get_results($str);
				?>
				<tbody>
					<?php
					foreach($result as $events){
						$post=$events;
						setup_postdata($post);


						?>
						<tr>
							<td class=""><?php  echo the_title();?></td>
							<td><?php echo $events->event_start_date ?></td>
							<td><?php echo $events->event_end_date ?></td>
							<td><a href="<?php echo get_site_url()?>/wp-admin/post.php?post=<?php echo $events->post_id?>&action=edit">Edit post</td></a>

						</tr>

						<?php 
					}

				}
				?>
			</tbody>
		</table>
	</div>
	<?php
}
?>

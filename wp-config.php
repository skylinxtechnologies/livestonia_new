<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'livestonia');
define('FS_METHOD','direct');
/** MySQL database username */
//define('DB_USER', 'd57082sa112762');
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
//define('DB_HOST', 'd57082.mysql.zone.ee');
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V9_Wm :Q,h,GJ21>rzJWU45d>^q[tq1^h~E;jm+?JKUslKH<+TarG8Kws@fY5:By');
define('SECURE_AUTH_KEY',  '}|d!`%c0_dyOCuSGe}+%OMFkR7vH2#4~IdcE7Bm)KOa5@ChptoD?GZ/&X<y!SG~)');
define('LOGGED_IN_KEY',    '79HYE<j-0,5L^SCE#nN11XuN`)VpQa]8K({M<J-f%3jQZw.n=b[9Nq?X=~Os= rJ');
define('NONCE_KEY',        'D+K.QFo T*uCW:?MjrIc=S6VZD*9Uk^vnu7.#@MTpzZJpL{vim!u}r@t|;(/Ily/');
define('AUTH_SALT',        'H13thC4h*H]eFTJp|gCWx^FRysn)(2QE;9*V}lmJWV-wm#GffN:s8ua[5_j<oK_@');
define('SECURE_AUTH_SALT', 'VR^mz@]NR?ae*HaF@$f~Z+(?xHNAMd&mM#.nN06rgw=_9`ve${Mr9h6l1@Z;fCtX');
define('LOGGED_IN_SALT',   'rW)w$He`NBdY.=},S}V4KcWRlEq1w/HGH)-!/PhT&9MZx;.3%r>]_jY_mTBfnEfN');
define('NONCE_SALT',       'XH^b[vOFA^.uf[<fpMhEz_`x{o8%GHtVP@:7HH&H{^f%K.jjKsQq#Rz-@ V2|N;x');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
